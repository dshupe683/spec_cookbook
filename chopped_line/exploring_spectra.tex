
 
\date{\today}
\title{PACS Spectroscopy Recipe: Inspecting a Pointed Chopped Line Spectroscopy Observation}

\documentclass[letterpaper,12pt, ]{article}
\usepackage[usenames,dvipsnames]{color}
\usepackage{tikz}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{wrapfig}
\usepackage{subcaption}
\usepackage{hyperref}

\textwidth=6.5in
\textheight=9.5in
\topmargin=-0.75in
\oddsidemargin=0.0in
\evensidemargin=0.0in

\pagestyle{myheadings}

\definecolor{light-gray}{gray}{0.90}
\definecolor{dark-gray}{gray}{0.4}
\definecolor{dark-blue}{RGB}{25,25,112}

\hypersetup{colorlinks=true,urlcolor=dark-blue}

\lstset{basicstyle=\normalsize\ttfamily,
        showstringspaces=false,
        basewidth=1.2ex,
        fontadjust=true,
        escapechar=�,
        backgroundcolor=\color{light-gray},
        stringstyle=\color{dark-gray},
        commentstyle=\color{black}
        numbers=left,
        numberstyle=\tiny,
        language=Python}

\newcommand{\roundbox}[1]{%
  \tikz[baseline=-1ex]%
  \node[%
  inner sep=1.5pt,
  draw=black,
  fill=black,
  text=white,
  rounded corners=2.5pt]{#1};}

\newcommand{\boxref}[1]{%
  \begingroup%
  \scriptsize\ttfamily%
  \roundbox{\ref{#1}}%
  \endgroup%
}

\newcount\mymark
\makeatletter
\def\mycoderef#1{%
  \global\advance\mymark by 1%
  \protected@write \@auxout {}{\string \newlabel {#1}{{\the\mymark}{}}}%
  \makebox[0pt][r]{{\scriptsize\roundbox{\the\mymark}~}}%
}
\makeatother



\begin{document}

\maketitle


\section{Introduction}\label{sec:intro}

This recipe covers the steps needed to explore and inspect a PACS chopped line spectroscopy observation
of a point source, HD 141569. The aim is to answer these three questions:
\begin{enumerate}
  \item{Is the source a point source, or is it extended?}
  \item{Is the source well-centered on the central spaxel, or not?}
  \item{Was the spectrometer chopped onto nearby emission?}
\end{enumerate}


Before embarking on the steps in this recipe, the reader must should have an installation of the 
\href{http://herschel.esac.esa.int/HIPE_download.shtml}{Herschel Interactive Processing Environment (HIPE)} 
available and should be familiar with the \href{http://herschel.esac.esa.int/hcss-doc-12.0/load/quickstart/html/quickstart.html}{HIPE Quickstart Guide}. The companion recipe ``Identifying and Accessing Data'' explains how to load the data for our example observation
into HIPE.

A companion script contains the all the processing commands, so it is not necessary to copy and paste from this document into HIPE.

The next steps explore the higher-level products.

\section{Text summary of the observation}\label{sec:textsummary}
A text listing of the observation is produced with
\lstinputlisting[language=Python, numbers=left, firstline=25, lastline=25, firstnumber=25]{exploring_spectra.py}
which yields detailed information about the observation. Check the output for the mode (Pointed, Chop/Nod for this recipe),
and the prime observing blocks (Camera=Blue, Band=B3A, O I 3P1-3P2, 63.18 micron). Check that you have set
the value of the ``camera'' variable correctly in the previous section.

\section{Source overlays on images}
Next, a WISE Band 3 (12$\mu$m) image is retrieved, and read into a variable ``image''. Then an overlay of the footprint is displayed using
\lstinputlisting[language=Python,numbers=left,firstline=76, lastline=76, firstnumber=76]{exploring_spectra.py}
\lstinputlisting[language=Python,numbers=left,firstline=79, lastline=82, firstnumber=79]{exploring_spectra.py}
with the result shown in Figure \ref{fig:overlay}.
\begin{figure}
\center\includegraphics[width=1.0\textwidth]{figures/hd141569_overlay_wise_pacs.pdf}
\caption{Top left: Overlay of spaxels (spatial pixels of the spectral cube) on a 2MASS J-band image. The spaxels are numbered as (row, column). Top right: spectrum at the central spaxel. The line at 63.32 $\mu$m is from ortho-water. Lower left: Overlay on 70 $\mu$m image. Lower right: Overlay on 160 $\mu$m image.}\label{fig:overlay}
\end{figure}
Similar methods are used to retrieve a PACS image of HD 141569 from the archive and to overlay the spaxel locations for
the ``on-source'' positions. Note that the central spaxel is fairly well-aligned with the images but that there is a
slight mis-pointing. Corrections for mis-pointing are discussed in detail in the companion recipe `
``Extracting a Spectrum and Line Parameters from a Point Source Observation''.

The pointing of the spaxel locations can be plotted from the Level 0.5 products. It is important to make sure
the spaxels are not being chopped onto emission. From the overlays and Figure \ref{fig:footprint}
\begin{figure}
\center\includegraphics[width=1.0\textwidth]{figures/hd141569_choppedoverlay.pdf}
\caption{PACS spaxel locations for our chopped and nodded observation of HD 141569 overlaid
on a 100 $\mu$m image from the archive, showing the chopping was not onto any nearby emission.}\label{fig:footprint}
\end{figure}
we can see that there is no danger of having chopped onto noticeable emission, and that the source is a point source.

\section{Versions of software and calibration}
Check the version of the pipeline and of the calibration tree, and compare it to the current HIPE version
and the calibration tree that HIPE has downloaded. The pipeline (SPG) version and the calibration tree
version are displayed by the \lstinline$obsSummary$ function in section \ref{sec:textsummary}. They are also available in
the metadata using
\lstinputlisting[language=Python, numbers=left, firstline=115, lastline=116, firstnumber=115]{exploring_spectra.py}
The HIPE version you are using can be checked from top-level menu Help:About. Both the software
and calibration versions can be checked with 
\lstinputlisting[language=Python,numbers=left,firstline=118, lastline=120, firstnumber=118]{exploring_spectra.py}

\section{Overview of all the spectra}

Finally, plot all the spectra in 5x5 format (Figure \ref{fig:5x5}) on a common scale with
\lstinputlisting[language=Python,numbers=left,firstline=123, lastline=136, firstnumber=123]{exploring_spectra.py}
\begin{figure}[t]
\center\includegraphics[width=\textwidth]{figures/hd141569_5x5.pdf}
\caption{5x5 plot of all spaxels in the O I 63$\mu$m line of HdD141569. Flux density is scaled from -0.5 to 10 Jy/pixel
and wavelength from 63.0 to 63.4 $\mu$m.  }\label{fig:5x5}
\end{figure}
Here a {\it spaxel} refers to a spatial pixel, i.e. the spatial unit of a spectral cube. The plot is also consistent
with a point source.

\section{Variation: A fancier 5x5 plot}

The rest of the companion script contains a much fancier version of the 5x5 plot. This plot overlays
the spaxels oriented on the sky, overlaid on a contour map of the PACS blue image, with the spectra
plotted in each spaxel. The output is show in Figure \ref{fig:fancy5x5}.
\begin{figure}[t]
\center\includegraphics[width=\textwidth]{figures/HD_141569_contour_spectra.pdf}
\caption{A fancier version of the 5x5 plot of all spaxels in the O I 63$\mu$m line of HD 141569. 
Note that the spaxels are centered on their correct positions on the sky, but are not rotated 
and are undersized relative to the true spaxel footprints in preceding figures. The contour plot is
from the PACS blue image. The small star denotes the ICRS coordinates of HD 141569A at epoch 2010.0.
The small black asterisk denotes the mean coordinates of the central spaxel. The difference is about
1.5 arcseconds in Right Ascension and 0.5 arcseconds in Declination. }
\label{fig:fancy5x5}
\end{figure}

\end{document}

