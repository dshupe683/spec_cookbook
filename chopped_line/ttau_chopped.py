#################################################################################################################
# Retrieve the observation using MyHSA
#################################################################################################################

Configuration.setProperty("hcss.ia.pal.pool.hsa.on_demand","True")
MyHSAPool.getInstance().setConnection(MyHSAPool.ONCACHED)
obsid=1342190353
obs = getObservation(obsid, useHsa=True)

workdir = Configuration.getProperty('var.hcss.workdir')

camera = 'blue'

verbose = True

#################################################################################################################
# Explore level 2  data
#################################################################################################################

# A) Summary of the observation
#    gives a summary of the observation, including the pipeline version used and calibration product version
obsSummary(obs)

# B) Extract the cube of rebinned data
level2 = PacsContext(obs.level2)
if (camera == 'blue'):
    cubes = level2.refs["HPS3DRB"].product
else:
    cubes = level2.refs["HPS3DRR"].product
cube = cubes.get(0)

# C) Check the pointing (footprint of image with WCS)

target = obs.meta["object"].value.replace(" ","_")

# Get a fits image from 2MASS
import os, urllib2
import urllib2

ra_str = herschel.calsdb.util.Coordinate.ra2String(obs.meta['ra'].value).replace(' ',':')
dec_str = herschel.calsdb.util.Coordinate.dec2String(obs.meta['dec'].value).replace(' ',':')
if obs.meta['dec'].value >= 0.0: dec_str = '+' + dec_str
imageurl = "http://irsa.ipac.caltech.edu/cgi-bin/Oasis/2MASSImg/nph-2massimg?" + \
    "objstr=%s%s&size=120&band=k"%(ra_str,dec_str)
imagefile = urllib2.urlopen(imageurl)
imagepath = os.path.join(workdir,target+'_2massk.fits')
output = open(imagepath,'wb')
output.write(imagefile.read())
output.close()

image = fitsReader(file = imagepath)
fp = pacsSpectralFootprint(image)
# Then the "fp" variable has some methods to add more footprints, or to 
# access the Display inside for e.g. custom annotations:
fp.addFootprint(cube) 
fp.display.zoomFactor = 4
fp.display.setColortable("Real", "Negative", "logarithmic")
fp.display.setCutLevels(99.0)

# Get PACS images for this target and make overlays
imobs = getObservation(1342228860,useHsa=True)
blueimage = imobs.refs["level2_5"].product.refs["HPPPMOSB"].product.refs["photProject"].product
redimage = imobs.refs["level2_5"].product.refs["HPPPMOSR"].product.refs["photProject"].product
fp1 = pacsSpectralFootprint(blueimage)
fp1.addFootprint(cube)
fp1.display.zoomFactor = 4
fp1.display.setColortable("Real", "NegativeLog", "logarithmic")
fp1.display.setCutLevels(99.0)
fp2 = pacsSpectralFootprint(redimage)
fp2.addFootprint(cube)
fp2.display.zoomFactor = 4
fp2.display.setColortable("Real", "NegativeLog", "logarithmic")
fp2.display.setCutLevels(99.0)

# D) Check calibration product version and HIPE version 
print level2.meta["creator"]
print level2.meta["calVersion"]
# Current versions (HIPE from menu   Help->about)
print Configuration.getProjectInfo().getVersion()
calTree = getCalTree(obs=obs)
print calTree.version

# E) check spectra (5x5 plot)
p1 = plotCube5x5(origcube)
for i in range(5):
    for j in range(5):
        p1.getPlot(i,j).xrange = [63.0,63.4]
        p1.getPlot(i,j).yrange = [-5,400]
        p1.getPlot(i,j).stroke = 3
        if (i==0): 
	    p1.getPlot(i,j).ytitle="Flux [Jy]"
	else:
	    p1.getPlot(i,j).ytitle=""
	if (j==4): 
	    p1.getPlot(i,j).xtitle="Wavelength [$\mu$m]"
	else:
	    p1.getPlot(i,j).xtitle=""

# only the central spaxel
x,y = 2,2
p = plotCubes(cubes, x=x, y=y,stroke=1)


#################################################################################################################
# Integrity check of level 0 data
#################################################################################################################

# A) extract level 0 data 
pacsPropagateMetaKeywords(obs,'0', obs.level0)
level0 = PacsContext(obs.level0)
slicedFrames  = SlicedFrames(level0.fitted.getCamera(camera).product)
slicedRawRamp = level0.raw.getCamera(camera).product
slicedDmcHead = level0.dmc.getCamera(camera).product    

# B) Plots of grating position, chopper position, raw signal for central pixel

# fix finetime  (bug in the system)
frame = slicedFrames.refs[0].product
t = frame.getStatus("FINETIME")
dt = t[1:]-t[:-1]
q = dt < 0
indminus = q.where(q)
if indminus.length() > 0:
  ind = Selection(q.where(q).toInt1d().add(1))
  indplus = Selection(q.where(q).toInt1d().add(2))
  t[ind] = (t[indminus]+t[indplus])/2
frame.setStatus("FINETIME",t)

# Plot grating position as function of time
gpr = slicedFrames.get(0).getStatus("GPR")
time = slicedFrames.get(0).getStatus("FINETIME")*1E-6
time -= time[0]
p = PlotXY(time,gpr,xtitle='Time [s]',ytitle='Grating position')

# Plot chopper position as function of time
cpr = slicedFrames.get(0).getStatus("CPR")
p = PlotXY(time,cpr,xtitle='Time [s]',ytitle='Chopper position',
    line=Style.NONE,symbol=Style.FSQUARE,symbolSize=1.5)

# Plot the raw signal of the central pixel
x,y = 8,12
signal = slicedFrames.get(0).signal
p = PlotXY(time,-signal[x,y,:],xtitle='Time [s]',ytitle='Flux [ADU]')

#################################################################################################################
# Pipeline
#################################################################################################################

# Prepare for multi-threading (default choices)
Configuration.setProperty("herschel.pacs.spg.common.superThreadCount","0")
Configuration.setProperty("herschel.pacs.spg.spec.threadCount","0")


# ------------------------------------------------------------------------------
#        Processing      Level 0 -> Level 0.5
# ------------------------------------------------------------------------------

slicedFrames = specFlagSaturationFrames(slicedFrames, rawRamp = slicedRawRamp, calTree=calTree, copy=1)
slicedFrames = specConvDigit2VoltsPerSecFrames(slicedFrames, calTree=calTree)
slicedFrames = detectCalibrationBlock(slicedFrames)
slicedFrames = addUtc(slicedFrames, obs.auxiliary.timeCorrelation)
slicedFrames = specAddInstantPointing(slicedFrames, obs.auxiliary.pointing, calTree = calTree, orbitEphem = obs.auxiliary.orbitEphemeris, horizonsProduct = obs.auxiliary.horizons)    
if (isSolarSystemObject(obs)):
  slicedFrames = correctRaDec4Sso (slicedFrames, timeOffset=0, orbitEphem=obs.auxiliary.orbitEphemeris, horizonsProduct=obs.auxiliary.horizons, linear=0)
slicedFrames = specExtendStatus(slicedFrames, calTree=calTree)
slicedFrames = convertChopper2Angle(slicedFrames, calTree=calTree)
slicedFrames = specAssignRaDec(slicedFrames, calTree=calTree)

# Check if the observation has been done correctly (footprint)
ppoint = slicedPlotPointingOnOff(slicedFrames)
s = slicedFrames.get(0)
p = PlotXY()
isSlew = s.getStatus('IsSlew')
onsource = s.getStatus('ONSOURCE')
offsource = s.getStatus('OFFSOURCE')
calsource = s.getStatus('CALSOURCE')
cal1 = calsource[1:]
cal0 = calsource[:-1]
q = (cal1 < 1) & (cal0 > 0)
start = q.where(q).toInt1d()[0]
postcal = onsource.copy()
postcal[:start]=0
postcal[start:]=1

q = onsource & postcal
green = q.where(q)
q = offsource & postcal
red = q.where(q)
q = isSlew & postcal
gray = q.where(q)
ra = Condense(0,16,MEDIAN)(s.ra[1:17,:,:])
dec= Condense(0,16,MEDIAN)(s.dec[1:17,:,:])

nred=red.length()
ngreen=green.length()
ngray=gray.length()
if ngray > 0:
  p.addLayer(LayerXY(ra[0,12,gray],dec[0,12,gray],line=Style.NONE,symbol=Style.VCROSS,color=java.awt.Color.gray,xtitle="RA",ytitle="Dec",name="Slewing"))
if ngreen > 0:
  p.addLayer(LayerXY(RESHAPE(ra[0,:,green]),RESHAPE(dec[0,:,green]),line=Style.NONE,symbol=Style.VCROSS,color=java.awt.Color.green,name="ON"))
if nred > 0:
  p.addLayer(LayerXY(RESHAPE(ra[0,:,red]),RESHAPE(dec[0,:,red]),line=Style.NONE,symbol=Style.VCROSS,color=java.awt.Color.red,name="OFF"))

p.addLayer(LayerXY(Double1d([s.meta["raNominal"].value]),Double1d([s.meta["decNominal"].value]),line=Style.NONE,symbol=Style.FSQUARE,color=java.awt.Color.red,name="Target"))

p.xaxis.type=Axis.RIGHT_ASCENSION
p.yaxis.type=Axis.DECLINATION
p.title.text="PACS footprint and slewing positions"
p.legend.visible=1


slicedFrames = waveCalc(slicedFrames, calTree=calTree)
slicedFrames = specCorrectHerschelVelocity(slicedFrames, obs.auxiliary.orbitEphemeris, obs.auxiliary.pointing, obs.auxiliary.timeCorrelation, obs.auxiliary.horizons)
slicedFrames = findBlocks(slicedFrames, calTree = calTree)
slicedFrames = specFlagBadPixelsFrames(slicedFrames, calTree=calTree)
# Slicing happens here
slicedFrames = pacsSliceContext(slicedFrames,[slicedDmcHead],removeUndefined=True, removeMasked=True)
slicedDmcHead = pacsSliceContext.additionalOutContexts[0]
slicedFrames = flagChopMoveFrames(slicedFrames, dmcHead=slicedDmcHead, calTree=calTree)
slicedFrames = flagGratMoveFrames(slicedFrames, dmcHead=slicedDmcHead, calTree=calTree)

# ------------------------------------------------------------------------------
#         Processing      Level 0.5 -> Level 1
# ------------------------------------------------------------------------------


slicedFrames = activateMasks(slicedFrames, String1d([" "]), exclusive = True)
slicedFrames = specFlagGlitchFramesQTest(slicedFrames,copy=1)


###################################################################################################
# Interactive step
###################################################################################################
# Inspection of glitches
slice = 1
MaskViewer(slicedFrames.get(slice))

# Find noisy pixels
f=slicedFrames.get(slice)
g = f.getMask("GLITCH")
ds = Double2d(18,25)
for i in range(18):
    for j in range(25): 
        q = g[i,j,:] == 0
        ds[i,j]=STDDEV(f.signal[i,j,q.where(q)])

q = ds > (MEDIAN(ds)+5*STDDEV(ds))
ind = q.where(q).toInt1d()
for i in range(ind.length()):
    print ind[i]/25, ind[i]%25, ds[ind[i]]

# Check them in the MaskViewer and eventually mask part of them
for i in range(0,2000): 
   f.setMask("GLITCH",6,0,i,True)

slicedFrames.replace(slice,f)
###################################################################################################


slicedFrames = activateMasks(slicedFrames, slicedFrames.get(0).getMaskTypes())
slicedFrames = convertSignal2StandardCap(slicedFrames, calTree=calTree)
calBlock = selectSlices(slicedFrames,scical="cal").get(0)
csResponseAndDark = specDiffCs(calBlock, calTree = calTree)
slicedFrames = specDiffChop(slicedFrames, scical = "sci", keepall = False, normalize=False)
slicedFrames = rsrfCal(slicedFrames, calTree=calTree)
slicedFrames = specRespCal(slicedFrames, csResponseAndDark = csResponseAndDark) 
slicedCubes = specFrames2PacsCube(slicedFrames)


###################################################################################################
#  Interactive step
###################################################################################################

### Spectral Flat-Fielding  #######################################################################

# 1. Flag outliers and rebin
waveGrid=wavelengthGrid(slicedCubes, oversample=2, upsample=3, calTree=calTree)
slicedCubes = activateMasks(slicedCubes, String1d(["GLITCH","UNCLEANCHOP","NOISYPIXELS","RAWSATURATION","SATURATION","GRATMOVE", "BADPIXELS"]), exclusive = True)
slicedCubes = specFlagOutliers(slicedCubes, waveGrid, nSigma=5, nIter=1)
slicedCubes = activateMasks(slicedCubes, String1d(["GLITCH","UNCLEANCHOP","NOISYPIXELS","RAWSATURATION","SATURATION","GRATMOVE", "OUTLIERS", "BADPIXELS"]), exclusive = True)
slicedRebinnedCubes = specWaveRebin(slicedCubes, waveGrid)

# 2. Mask the spectral lines
# Here we can define the list of lines to mask or choose to go automatically
widthDetect,threshold =  2.5,10
print obs.meta["lines"].string

 # use an empty list for automatic detection
lineList=[63.18,63.32] 
slicedCubesMask = maskLines(slicedCubes,slicedRebinnedCubes, lineList=lineList, widthDetect=widthDetect, widthMask=2.5, threshold=threshold, copy=1, verbose=verbose, maskType="INLINE",calTree=calTree)

# 3. Actual spectral flatfielding
# Set slopeInContinuum  to true for lines existing on a continuum with a significant spectral slope. 
# Set scaling to true (highly recommended), a multiplicative correction is applied. If false, it is additive. 
slopeInContinuum = 1
slicedCubes = specFlatFieldLine(slicedCubesMask, scaling=1, copy=1, maxrange=[50.,230.], slopeInContinuum=slopeInContinuum, maxScaling=2., maskType="OUTLIERS_FF", offset=0,calTree=calTree,verbose=verbose)

# 4. Rename mask OUTLIERS to OUTLIERS_B4FF (specFlagOutliers would refuse to overwrite OUTLIERS) & deactivate mask INLINE
slicedCubes.renameMask("OUTLIERS", "OUTLIERS_B4FF")
slicedCubes = deactivateMasks(slicedCubes, String1d(["INLINE", "OUTLIERS_B4FF"]))
if verbose: maskSummary(slicedCubes, slice=0)

# 5. Remove intermediate results
del waveGrid, slicedRebinnedCubes, slicedCubesMask

# ------------------------------------------------------------------------------
#         Processing      Level 1 -> Level 2
# ------------------------------------------------------------------------------


# Interactive step !!!
# The user can select his optimal sampling
oversample,upsample = 2,4
waveGrid=wavelengthGrid(slicedCubes, oversample=oversample, upsample=upsample, calTree = calTree)
# Outlier flagging
slicedCubes = activateMasks(slicedCubes, String1d(["SATURATION","RAWSATURATION","NOISYPIXELS","BADPIXELS","UNCLEANCHOP","GRATMOVE","GLITCH"]), exclusive = True)
slicedCubes = specFlagOutliers(slicedCubes, waveGrid, nSigma=5, nIter=1)
# Rebinning
slicedCubes = activateMasks(slicedCubes, String1d(["OUTOFBAND","SATURATION","RAWSATURATION","NOISYPIXELS","BADPIXELS","UNCLEANCHOP","GRATMOVE","GLITCH", "OUTLIERS"]), exclusive = True)
slicedRebinnedCubes = specWaveRebin(slicedCubes, waveGrid)
# Adding nods
slicedFinalCubes = specAddNodCubes(slicedRebinnedCubes)

# ------------------------------------------------------------------------------
#         Processing Level 2.0
# ------------------------------------------------------------------------------

# Obtain projected cubes
slicedProjectedCubes = specProject(slicedFinalCubes,outputPixelsize=3.0)


# Aperture correction to central spaxel of rebinned cubes
applyPointSourceCorrection = 1
target = slicedFinalCubes.meta["object"].value.replace(" ","_")

# Check the lines in the cubes
slicedSummary(slicedFinalCubes)

# If the line is in slice 0
slice = 0
    
# Aperture correction using only the central spaxel (used for faint sources)
# a. Extract central spectrum
# b. Apply the point source correction
correct3x3 = 0
name = "OBSID_"+str(obsid)+"_"+target+"_"+camera+"_centralSpectrum_PointSourceCorrected_Corrected3x3NO_slice_"
centralSpectrum = extractCentralSpectrum(slicedFinalCubes, slice=slice,noNaNs=1,correct3x3=correct3x3, width=0, applyPointSourceCorrection=applyPointSourceCorrection, verbose=verbose, calTree=calTree)
simpleFitsWriter(product=centralSpectrum,file = name+str(slice).zfill(2)+".fits")

# Aperture correction using the 3x3 central spaxels (used for bright sources)
# a. Extract central spectrum
# b. Apply a simple flux correction by comparison between the central spaxel and its 8 neighbours
correct3x3 = 1
applyPointSourceCorrection = 1
name = "OBSID_"+str(obsid)+"_"+target+"_"+camera+"_centralSpectrum_PointSourceCorrected_Corrected3x3YES_slice_"
centralSpectrumCorr3x3 = extractCentralSpectrum(slicedFinalCubes, slice=slice,noNaNs=1,correct3x3=correct3x3, width=0, applyPointSourceCorrection=applyPointSourceCorrection, verbose=verbose, calTree=calTree)
simpleFitsWriter(product=centralSpectrumCorr3x3,file = name+str(slice).zfill(2)+".fits")


# Compare fluxes
origcube = cube
cube = slicedFinalCubes.get(slice)


p = PlotXY()
p.addLayer(LayerXY(cube.wave,cube.flux[:,2,2],xtitle="wavelength [um]",
    ytitle="Flux [Jy/pixel]",stroke=2))
p.addLayer(LayerXY(origcube.wave,origcube.flux[:,2,2],stroke=2))
p.addLayer(LayerXY(centralSpectrum.wave,centralSpectrum.flux,stroke=2))
p.addLayer(LayerXY(centralSpectrumCorr3x3.wave,centralSpectrumCorr3x3.flux,
    stroke=2))
p.getLegend().setVisible(1)
p.getSubPlot(0).getLayer(0).setName(u'no corr')
p.getSubPlot(0).getLayer(1).setName(u'v10.3.0 no corr')
p.getSubPlot(0).getLayer(2).setName(u'ap corr')
p.getSubPlot(0).getLayer(3).setName(u'3x3 corr')


# Explore with Spectrum Explorer
centralSpec = centralSpectrumCorr3x3.spectrum1d
openVariable("centralSpec", "Spectrum Explorer")





