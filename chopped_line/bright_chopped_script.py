#################################################################################################################
# Retrieve the observation
#################################################################################################################

obsid=1342186798
useHsa = 0
obs    = getObservation(obsid, verbose=True, useHsa=useHsa, poolLocation=None, poolName=None)

#################################################################################################################
# Explore level 2  data
#################################################################################################################

# A) Summary of the observation
#    gives a summary of the observation, including the pipeline version used and calibration product version
obsSummary(obs)

# B) Extract the cube of rebinned red data
level2 = PacsContext(obs.level2)
redcubes = level2.refs["HPS3DRR"].product
redcube = redcubes.get(0)

# C) Check the pointing (footprint of image with WCS)

#M82_SDSS_r = fitsReader(file = '/home/fadda/workspace/PACS/Cookbooks/M82_SDSS_r.fits')
#fp = pacsSpectralFootprint(M82_SDSS_r)
# wget -O m82.fits "http://www1.cadc-ccda.hia-iha.nrc-cnrc.gc.ca/cadcbin/dss/dss-server.py?ra=09:55:52.7&dec=+69:40:46&mime-type=applications/x-fits&x=15&y=15"

# Get a fits image from DSS
import os
#string = "wget -O m82.fits 'http://www1.cadc-ccda.hia-iha.nrc-cnrc.gc.ca/cadcbin/dss/dss-server.py?ra=09:55:52.7&dec=+69:40:46&mime-type=applications/x-fits&x=15&y=15'"
string = "wget -O m82.fits 'http://irsa.ipac.caltech.edu/cgi-bin/Oasis/2MASSImg/nph-2massimg?objstr=09:55:52.7 +69:40:46&size=120&band=j'"

print os.popen(string).read()
print os.popen("ls *fits").read()

dir = os.getcwd()
image = fitsReader(file = dir+'/m82.fits')
fp = pacsSpectralFootprint(image)
# Then the "fp" variable has some methods to add more footprints, or to 
# access the Display inside for e.g. custom annotations:
fp.addFootprint(redcube) 
fp.display.zoomFactor = 2
fp.display.setColortable("Real", "Negative", "linear")
fp.display.setCutLevels(99.0)


# D) Check calibration product version and HIPE version 
print level2.meta["creator"]
print level2.meta["calVersion"]
# Current versions (HIPE from menu   Help->about)
calTree = getCalTree(obs=obs)
print calTree.version

# E) check spectra (5x5 plot)
p = plotCube5x5(redcube)

# only one pixel
x,y = 2,2
p = plotCubes(redcubes, x=x, y=y,stroke=1)

# plot with error bands
p = plotCubesStddev(redcubes, plotLowExp=1, plotDev=0, nsigma=3, spaxelX=x, spaxelY=y, verbose=verbose, calTree=calTree)


#################################################################################################################
# Integrity check of level 0 data
#################################################################################################################

# A) extract level 0 data for the red camera
camera = 'red'
pacsPropagateMetaKeywords(obs,'0', obs.level0)
level0 = PacsContext(obs.level0)
slicedFrames  = SlicedFrames(level0.fitted.getCamera(camera).product)
slicedRawRamp = level0.raw.getCamera(camera).product
slicedDmcHead = level0.dmc.getCamera(camera).product    

# B) Summary, plot of grating position, plot of raw signal for central pixel
slicedSummary(slicedFrames)

# fix finetime  (bug in the system)
frame = slicedFrames.refs[0].product
t = frame.getStatus("FINETIME")
dt = t[1:]-t[:-1]
q = dt < 0
indminus = q.where(q)
if indminus.length() > 0:
  ind = Selection(q.where(q).toInt1d().add(1))
  indplus = Selection(q.where(q).toInt1d().add(2))
  t[ind] = (t[indminus]+t[indplus])/2
frame.setStatus("FINETIME",t)

# Plot grating position as function of time
gpr = slicedFrames.get(0).getStatus("GPR")
time = slicedFrames.get(0).getStatus("FINETIME")*1E-6
time -= time[0]
p = PlotXY(time,gpr,xtitle='Time [s]',ytitle='Grating position')

# Plot chopper position as function of time
cpr = slicedFrames.get(0).getStatus("CPR")
p = PlotXY(time,cpr,xtitle='Time [s]',ytitle='Chopper position',line=Style.NONE,symbol=Style.DOT)

# Plot the raw signal of the central pixel
x,y = 8,12
signal = slicedFrames.get(0).signal
p = PlotXY(time,-signal[x,y,:],xtitle='Time [s]',ytitle='Flux [ADU]')

#################################################################################################################
# Pipeline
#################################################################################################################

# Prepare for multi-threading (default choices)
Configuration.setProperty("herschel.pacs.spg.common.superThreadCount","0")
Configuration.setProperty("herschel.pacs.spg.spec.threadCount","0")


# ------------------------------------------------------------------------------
#        Processing      Level 0 -> Level 0.5
# ------------------------------------------------------------------------------

slicedFrames = specFlagSaturationFrames(slicedFrames, rawRamp = slicedRawRamp, calTree=calTree, copy=1)
slicedFrames = specConvDigit2VoltsPerSecFrames(slicedFrames, calTree=calTree)
slicedFrames = detectCalibrationBlock(slicedFrames)
slicedFrames = addUtc(slicedFrames, obs.auxiliary.timeCorrelation)
slicedFrames = specAddInstantPointing(slicedFrames, obs.auxiliary.pointing, calTree = calTree, orbitEphem = obs.auxiliary.orbitEphemeris, horizonsProduct = obs.auxiliary.horizons)    
if (isSolarSystemObject(obs)):
  slicedFrames = correctRaDec4Sso (slicedFrames, timeOffset=0, orbitEphem=obs.auxiliary.orbitEphemeris, horizonsProduct=obs.auxiliary.horizons, linear=0)
slicedFrames = specExtendStatus(slicedFrames, calTree=calTree)
slicedFrames = convertChopper2Angle(slicedFrames, calTree=calTree)
slicedFrames = specAssignRaDec(slicedFrames, calTree=calTree)

# Check if the observation has been done correctly (footprint)
ppoint = slicedPlotPointingOnOff(slicedFrames)

slicedFrames = waveCalc(slicedFrames, calTree=calTree)
slicedFrames = specCorrectHerschelVelocity(slicedFrames, obs.auxiliary.orbitEphemeris, obs.auxiliary.pointing, obs.auxiliary.timeCorrelation, obs.auxiliary.horizons)
slicedFrames = findBlocks(slicedFrames, calTree = calTree)
slicedFrames = specFlagBadPixelsFrames(slicedFrames, calTree=calTree)
# Slicing happens here
slicedFrames = pacsSliceContext(slicedFrames,[slicedDmcHead],removeUndefined=True, removeMasked=True)
slicedDmcHead = pacsSliceContext.additionalOutContexts[0]
slicedFrames = flagChopMoveFrames(slicedFrames, dmcHead=slicedDmcHead, calTree=calTree)
slicedFrames = flagGratMoveFrames(slicedFrames, dmcHead=slicedDmcHead, calTree=calTree)

# ------------------------------------------------------------------------------
#         Processing      Level 0.5 -> Level 1
# ------------------------------------------------------------------------------


slicedFrames = activateMasks(slicedFrames, String1d([" "]), exclusive = True)
slicedFrames = specFlagGlitchFramesQTest(slicedFrames,copy=1)


###################################################################################################
# Interactive step
###################################################################################################
# Inspection of glitches
slice = 1
MaskViewer(slicedFrames.get(slice))

# Find noisy pixels
f=slicedFrames.get(slice)
g = f.getMask("GLITCH")
ds = Double2d(18,25)
for i in range(18):
    for j in range(25): 
        q = g[i,j,:] == 0
        ds[i,j]=STDDEV(f.signal[i,j,q.where(q)])

q = ds > (MEDIAN(ds)+5*STDDEV(ds))
ind = q.where(q).toInt1d()
for i in range(ind.length()):
    print ind[i]/25, ind[i]%25, ds[ind[i]]

# Check them and eventually mask part of them
# for i in range(1400,1600): 
#    f.setMask("GLITCH",13,0,i,True)

# slicedFrames.replace(slice,f)
###################################################################################################


slicedFrames = activateMasks(slicedFrames, slicedFrames.get(0).getMaskTypes())
slicedFrames = convertSignal2StandardCap(slicedFrames, calTree=calTree)
calBlock = selectSlices(slicedFrames,scical="cal").get(0)
csResponseAndDark = specDiffCs(calBlock, calTree = calTree)
slicedFrames = specDiffChop(slicedFrames, scical = "sci", keepall = False, normalize=False)
slicedFrames = rsrfCal(slicedFrames, calTree=calTree)
slicedFrames = specRespCal(slicedFrames, csResponseAndDark = csResponseAndDark) 
slicedCubes = specFrames2PacsCube(slicedFrames)


###################################################################################################
#  Interactive step
###################################################################################################

### Spectral Flat-Fielding  #######################################################################

# 1. Flag outliers and rebin
waveGrid=wavelengthGrid(slicedCubes, oversample=2, upsample=3, calTree=calTree)
slicedCubes = activateMasks(slicedCubes, String1d(["GLITCH","UNCLEANCHOP","NOISYPIXELS","RAWSATURATION","SATURATION","GRATMOVE", "BADPIXELS"]), exclusive = True)
slicedCubes = specFlagOutliers(slicedCubes, waveGrid, nSigma=5, nIter=1)
slicedCubes = activateMasks(slicedCubes, String1d(["GLITCH","UNCLEANCHOP","NOISYPIXELS","RAWSATURATION","SATURATION","GRATMOVE", "OUTLIERS", "BADPIXELS"]), exclusive = True)
slicedRebinnedCubes = specWaveRebin(slicedCubes, waveGrid)

# 2. Mask the spectral lines
# Here we can define the list of lines to mask or choose to go automatically
widthDetect,threshold =  2.5,10
print obs.meta["lines"].string

lineList=[157.84781196989954,145.61851704000003,121.9825263,205.31890686000003]
# lineList=[]   for automatic detection
slicedCubesMask = maskLines(slicedCubes,slicedRebinnedCubes, lineList=lineList, widthDetect=widthDetect, widthMask=2.5, threshold=threshold, copy=1, verbose=verbose, maskType="INLINE",calTree=calTree)

# 3. Actual spectral flatfielding
# Set slopeInContinuum  to true for lines existing on a continuum with a significant spectral slope. 
# Set scaling to true (highly recommended), a multiplicative correction is applied. If false, it is additive. 
slopeInContinuum = 1
slicedCubes = specFlatFieldLine(slicedCubesMask, scaling=1, copy=1, maxrange=[50.,230.], slopeInContinuum=slopeInContinuum, maxScaling=2., maskType="OUTLIERS_FF", offset=0,calTree=calTree,verbose=verbose)

# 4. Rename mask OUTLIERS to OUTLIERS_B4FF (specFlagOutliers would refuse to overwrite OUTLIERS) & deactivate mask INLINE
slicedCubes.renameMask("OUTLIERS", "OUTLIERS_B4FF")
slicedCubes = deactivateMasks(slicedCubes, String1d(["INLINE", "OUTLIERS_B4FF"]))
if verbose: maskSummary(slicedCubes, slice=0)

# 5. Remove intermediate results
del waveGrid, slicedRebinnedCubes, slicedCubesMask

# ------------------------------------------------------------------------------
#         Processing      Level 1 -> Level 2
# ------------------------------------------------------------------------------


# Interactive step !!!
# The user can select his optimal sampling
oversample,upsample = 2,4
waveGrid=wavelengthGrid(slicedCubes, oversample=oversample, upsample=upsample, calTree = calTree)
# Outlier flagging
slicedCubes = activateMasks(slicedCubes, String1d(["SATURATION","RAWSATURATION","NOISYPIXELS","BADPIXELS","UNCLEANCHOP","GRATMOVE","GLITCH"]), exclusive = True)
slicedCubes = specFlagOutliers(slicedCubes, waveGrid, nSigma=5, nIter=1)
# Rebinning
slicedCubes = activateMasks(slicedCubes, String1d(["OUTOFBAND","SATURATION","RAWSATURATION","NOISYPIXELS","BADPIXELS","UNCLEANCHOP","GRATMOVE","GLITCH", "OUTLIERS"]), exclusive = True)
slicedRebinnedCubes = specWaveRebin(slicedCubes, waveGrid)
# Adding nodes
slicedFinalCubes = specAddNodCubes(slicedRebinnedCubes)

# ------------------------------------------------------------------------------
#         Processing Level 2.0
# ------------------------------------------------------------------------------

# Obtain projected cubes
slicedProjectedCubes = specProject(slicedFinalCubes,outputPixelsize=3.0)


# Aperture correction to central spaxel of rebinned cubes
applyPointSourceCorrection = 1
target = slicedFinalCubes.meta["object"].value.replace(" ","_")

# Check the lines in the cubes
slicedSummary(slicedFinalCubes)

# If the line is in slice 2
slice = 2
    
# Aperture correction using only the central spaxel (used for faint sources)
# a. Extract central spectrum
# b. Apply the point source correction
correct3x3 = 0
name = "OBSID_"+str(obsid)+"_"+target+"_"+camera+"_centralSpectrum_PointSourceCorrected_Corrected3x3NO_slice_"
centralSpectrum = extractCentralSpectrum(slicedFinalCubes, slice=slice,noNaNs=1,correct3x3=correct3x3, width=0, applyPointSourceCorrection=applyPointSourceCorrection, verbose=verbose, calTree=calTree)
simpleFitsWriter(product=centralSpectrum,file = name+str(slice).zfill(2)+".fits")

# Aperture correction using the 3x3 central spaxels (used for bright sources)
# a. Extract central spectrum
# b. Apply a simple flux correction by comparison between the central spaxel and its 8 neighbours
correct3x3 = 1
applyPointSourceCorrection = 0
name = "OBSID_"+str(obsid)+"_"+target+"_"+camera+"_centralSpectrum_PointSourceCorrected_Corrected3x3YES_slice_"
centralSpectrumCorr3x3 = extractCentralSpectrum(slicedFinalCubes, slice=slice,noNaNs=1,correct3x3=correct3x3, width=0, applyPointSourceCorrection=applyPointSourceCorrection, verbose=verbose, calTree=calTree)
simpleFitsWriter(product=centralSpectrumCorr3x3,file = name+str(slice).zfill(2)+".fits")


# Compare fluxes
cube = slicedFinalCubes.get(slice)


p = PlotXY()
p.addLayer(LayerXY(cube.wave,cube.flux[:,2,2],xtitle="wavelength [um]",ytitle="Flux [Jy/pixel]"))
p.addLayer(LayerXY(centralSpectrum.wave,centralSpectrum.flux))
p.addLayer(LayerXY(centralSpectrumCorr3x3.wave,centralSpectrumCorr3x3.flux))
p.getLegend().setVisible(1)
p.getSubPlot(0).getLayer(0).setName(u'original')
p.getSubPlot(0).getLayer(1).setName(u'ap corr')
p.getSubPlot(0).getLayer(2).setName(u'3x3 corr')

# Explore with Spectrum Explorer
centralSpec = centralSpectrumCorr3x3.spectrum1d
openVariable("centralSpec", "Spectrum Explorer")





