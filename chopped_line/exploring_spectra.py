#################################################################################################################
# Retrieve the observation
#################################################################################################################

obsid=1342190376
obs = getObservation(obsid)


#################################################################################################################
# Set up working directory, camera, and verbosity
#################################################################################################################

workdir = Configuration.getProperty('var.hcss.workdir')

camera = 'blue'

verbose = True

#################################################################################################################
# Explore level 2  data
#################################################################################################################

# A) Summary of the observation
#    gives a summary of the observation, including the pipeline version used and calibration product version
obsSummary(obs, forceUpdate=True)

# B) Extract the cube of rebinned data
level2 = PacsContext(obs.level2)
if (camera == 'blue'):
    cubes = level2.refs["HPS3DRB"].product
else:
    cubes = level2.refs["HPS3DRR"].product
cube = cubes.get(0)

# C) Check the pointing (footprint of image with WCS)

target = obs.meta["object"].value.replace(" ","_")
ra = obs.meta['ra'].value
dec = obs.meta['dec'].value

import os, urllib2
import urllib2

# Get a FITS image from WISE
irsaurl='http://irsa.ipac.caltech.edu/ibe/sia/wise/allsky/4band_p3am_cdd'+ \
     '?POS=%f,%f&SIZE=0.1&mcen'%(ra,dec)
xmlresult = urllib2.urlopen(irsaurl).read()
stbuilder = herschel.ia.vo.votable.StarTableBuilder(herschel.ia.vo.votable.StarTableBuilder.MODE.valueOf("MEMORY"))
stable = stbuilder.makeStarTableFromVOTable(xmlresult,0)
wiseband = 3 # WISE Band 3
wiseurl = stable.getCell(wiseband-1,1) # Band 3 from WISE
imagefile =  urllib2.urlopen(wiseurl)
imagepath = os.path.join(workdir,target+'_wise_band%d.fits'%wiseband)
output = open(imagepath,'wb')
output.write(imagefile.read())
output.close()
image = fitsReader(file = imagepath)
# Crop the image and save it
halfsize = 2./60. # 4 arcmin on a side
row1,col1 = image.wcs.getPixelCoordinates(\
        ra+halfsize/COS(dec*3.14159/180.),dec-halfsize)
row2,col2 = image.wcs.getPixelCoordinates(\
        ra-halfsize/COS(dec*3.14159/180.),dec+halfsize)
image = crop(image,int(row1),int(col1),int(row2),int(col2))

# Alternative: Get a fits image from 2MASS
#imageurl = "http://irsa.ipac.caltech.edu/cgi-bin/Oasis/2MASSImg/nph-2massimg?" + \
#    "objstr=%f,%f&size=120&band=j"%(obs.meta['ra'].value+0.01,obs.meta['dec'].value)
#imagefile = urllib2.urlopen(imageurl)
#imagepath = os.path.join(workdir,target+'_2massj.fits')
#output = open(imagepath,'wb')
#output.write(imagefile.read())
#output.close()


fp = pacsSpectralFootprint(image)
# Then the "fp" variable has some methods to add more footprints, or to 
# access the Display inside for e.g. custom annotations:
fp.addFootprint(cube) 
fp.display.zoomFactor = 6
fp.display.setColortable("Real", "Negative", "logarithmic")
fp.display.setCutLevels(90.0)

# Get PACS images for this target and make overlays
imobs = getObservation(1342215382,useHsa=True,version=11)
blueimage = imobs.refs["level2_5"].product.refs["HPPPMOSB"].product.refs["photProject"].product
redimage = imobs.refs["level2_5"].product.refs["HPPPMOSR"].product.refs["photProject"].product
fp1 = pacsSpectralFootprint(blueimage)
fp1.addFootprint(cube)
fp1.display.zoomFactor = 4
fp1.display.setColortable("Real", "Ramp", "linear")
fp1.display.setCutLevels(99.5)
fp2 = pacsSpectralFootprint(redimage)
fp2.addFootprint(cube)
fp2.display.zoomFactor = 4
fp2.display.setColortable("Real", "Ramp", "linear")
fp2.display.setCutLevels(99.5)

# plot the footprint from Level 0.5
if (camera == 'blue'):
    slicedFrames = obs.level0_5.refs["HPSFITB"].product
else:
    slicedFrames = obs.level2.refs["HPSFITR"].product
#ppoint = slicedPlotPointing(slicedFrames)
#ppoint.setFrameTitle("slicedPlotPointing - "+str(obsid)+" "+str(camera))
fp = pacsSpectralFootprint(blueimage)
fp.addFootprint(slicedFrames)
fp.display.zoomFactor = 4
fp.display.setColortable("Real", "Ramp", "linear")
fp.display.setCutLevels(99.5)
fp.removeFootprint(0)
fp.removeFootprint(0)

# D) Check calibration product version and HIPE version 
print level2.meta["creator"]
print level2.meta["calVersion"]
# Current versions (HIPE from menu   Help->about)
print Configuration.getProjectInfo().getVersion()
calTree = getCalTree(obs=obs)
print calTree.version

# E) check spectra (5x5 plot)
p1 = plotCube5x5(cube)
for i in range(5):
    for j in range(5):
        p1.getPlot(i,j).xrange = [63.0,63.4]
        p1.getPlot(i,j).yrange = [-0.5,10]
        p1.getPlot(i,j).stroke = 3
        if (i==0): 
	    p1.getPlot(i,j).ytitle="Flux [Jy]"
	else:
	    p1.getPlot(i,j).ytitle=""
	if (j==4): 
	    p1.getPlot(i,j).xtitle="Wavelength [$\mu$m]"
	else:
	    p1.getPlot(i,j).xtitle=""

# plot the central spaxel
x,y = 2,2
p = plotCubes(cubes, x=x, y=y,stroke=1)


##### Rotated spaxel spectra on contour plot

# Coordinates in ICRS epoch J2010.0 from SIMBAD
ra_true = 237.49056479 
dec_true = -03.92125985
ra_center = MEAN(cube.getRa()[:,2,2])
dec_center = MEAN(cube.getDec()[:,2,2])

# Import some useful classes
from java.awt import Color
from java.lang import Math
from herschel.share.fltdyn.math import SexagesimalFormatter
from herschel.share.fltdyn.math.SexagesimalFormatter import Mode

# Extract the blue map
map=blueimage
 
# Create the layer with the image
layIma=LayerImage(map.image)

# Create a PlotXY object
myPlot=PlotXY()

# Add the image layer to the plot
myPlot.addLayer(layIma)

#### Coordinates ####

# The image will be plotted in a reference system with origin in the lower-left
# corner of the image pixel [0,0] and with pixel size of 1x1.
#
# It is possible to change the position of the image respect to the PlotXY axes
# and change the pixel size, so that the PlotXY axes will show the 
# Right Ascension and the Declination coordinates.
#
# The LayerImage provides methods to set the position of the image and the
# pixel size with a system similar to the FITS WCS. 
#
# Please note:
# 1) PlotXY CANNOT rotate the image, so if you need to plot a map that
#    is not aligned with the North on RA and Dec axis, you will need to rotate it 
#    before plotting it. In this script we assume that the map is aligned with
#    the North.
#
# 2) The PlotXY axis system assumes that coordinates are a linear
#    transformation of pixel coordinates. So coordinates on the plotted axes
#    are not fully correct in some projections. They are correct only for small
#    angles near the projection reference.

# Extract the WCS of the map and put some WCS info into variables
wcs=map.wcs

crpix1=wcs.crpix1
crpix2=wcs.crpix2

crval1=wcs.crval1
crval2=wcs.crval2

cdelt1=wcs.cdelt1
cdelt2=wcs.cdelt2

naxis1=wcs.naxis1
naxis2=wcs.naxis2

# cos(Dec)
cosd=Math.cos(Math.toRadians(crval2))

# Set the origin and the scale of the axes so that they coincide with the WCS.
myPlot[0].xcdelt=cdelt1/cosd # note the cos(Dec)!!!
myPlot[0].ycdelt=cdelt2

myPlot[0].xcrpix=crpix1
myPlot[0].ycrpix=crpix2

myPlot[0].xcrval=crval1
myPlot[0].ycrval=crval2

# Change the axis type so that we have ticks in degrees/hours, min, sec
# and the RA growing toward the left.
myPlot.xaxis.type=Axis.RIGHT_ASCENSION
myPlot.yaxis.type=Axis.DECLINATION
myPlot.xaxis.titleText="Right Ascension (J2000)"
myPlot.yaxis.titleText="Declination (J2000)"
# Adjust ticks to be nicer
myPlot.xaxis.tick.autoAdjustNumber=0
myPlot.xaxis.tick.number=5
myPlot.xaxis.tick.minorNumber=3
myPlot.xaxis.getAuxAxis(0).tick.autoAdjustNumber=0
myPlot.xaxis.getAuxAxis(0).tick.number=5
myPlot.xaxis.getAuxAxis(0).tick.minorNumber=3
myPlot.yaxis.tick.autoAdjustNumber=0
myPlot.yaxis.tick.number=5
myPlot.yaxis.tick.minorNumber=3
myPlot.yaxis.getAuxAxis(0).tick.autoAdjustNumber=0
myPlot.yaxis.getAuxAxis(0).tick.number=5
myPlot.yaxis.getAuxAxis(0).tick.minorNumber=3

# Set the axes ranges so that the image fills completely the plotting area
xrange=[crval1-(crpix1-0.5)*cdelt1/cosd,\
          crval1-(crpix1-naxis1-0.5)*cdelt1/cosd]
myPlot.xrange=xrange
yrange=[crval2-(crpix2-0.5)*cdelt2,\
          crval2-(crpix2-naxis2-0.5)*cdelt2]
myPlot.yrange=yrange

# Change the size of the plotting area so that proportions are as on the sky
myPlot.setPlotSize(6.0,6.0*(naxis2*-cdelt2)/(naxis1*cdelt1))

#### Colours and intensity manipulation ####

# Set the colour table to have a grey image and the intensity table to have
# sources in black and empty sky in white
myPlot[0].colorTable="Ramp"
myPlot[0].intensityTable="Negative"

# Set the intensity range
peakval = MAX(map.image[map.image.where(IS_FINITE(map.image))])
highCut=peakval
lowCut=peakval*0.1
myPlot[0].highCut=highCut
myPlot[0].lowCut=lowCut

# Add the True position
myPlot.addLayer(LayerXY(Double1d([ra_true]),Double1d([dec_true]),\
      style=Style(line=Style.NONE,symbol=13,color=Color.red)))
myPlot.addLayer(LayerXY(Double1d([ra_center]),Double1d([dec_center]),\
      style=Style(line=Style.NONE,symbol=4,color=Color.black)))

#### Contours #####

# We want to draw level contours. We don't have (yet) a specialized layer for
# contours, so we will need to plot each contour segment.

# Generate the contours 
contours = automaticContour(image=map,levels=2,min=0.5*peakval,max=0.8*peakval,
     distribution=0)

# Plot the contours as 
keys=contours.keySet()
for key in keys:
   if key.startswith("Contour"):
      cont=contours[key]
      keysc=cont.keySet()
      for keyc in keysc:
          x=(cont[keyc].data[:,1]-crpix1+1.0)*cdelt1/cosd+crval1
          y=(cont[keyc].data[:,0]-crpix2+1.0)*cdelt2+crval2
          cLayer = LayerXY(x,y,color=Color.red)
          cLayer.setStroke(0.7)
          myPlot.addLayer(cLayer)
pass

# Remove the image layer
myPlot.removeLayer(0)
# Reset the range
# Set the axes ranges so that the spaxels span the plotting area
halfsize=0.5/60.0
xrange=[ra+halfsize/cosd,\
          ra-halfsize/cosd]
myPlot.xrange=xrange
yrange=[dec-halfsize,\
          dec+halfsize]
myPlot.yrange=yrange

# Change the size of the plotting area so that proportions are as on the sky
myPlot.setPlotSize(8.0,8.0)

# You have to follow this additional step for a plot to accept subplots.
myPlot.setLayout(PlotOverlayLayout())

### Overplot spectra of spaxels in rebinned cube
ylim = [-1,10]
xlim = [cube.meta['minWave'].value+0.1,cube.meta['maxWave'].value-0.1]
startSubs = 0
for i in [2,0,1,3,4]:
  for j in [2,0,1,3,4]:
    cubeDim = cube.getRa().dimensions
    spaxelRa = cube.getRa()[cubeDim[0]/2,i,j]
    spaxelDec  = cube.getDec()[cubeDim[0]/2,i,j]
    spaxhalf = 0.75*9.4/(2*3600.) # 0.75* Spaxel half-size in degrees
    # Normalize to xrange and yrange
    xspan = myPlot.xrange[0] - myPlot.xrange[1]
    yspan = myPlot.yrange[1] - myPlot.yrange[0]
    right = (myPlot.xrange[0] - (spaxelRa - spaxhalf/cosd))/xspan
    left = (spaxelRa + spaxhalf/cosd - myPlot.xrange[1])/xspan
    bottom = (spaxelDec - spaxhalf - myPlot.yrange[0])/yspan
    top = (myPlot.yrange[1] - (spaxelDec + spaxhalf))/yspan
    subplot = SubPlot(SubPlotBoundsConstraints(top,left,bottom,right))
    sLayer = LayerXY(cube.wave,cube.flux[:,i,j], color=Color.blue)
    sLayer.style=Style(chartType=Style.HISTOGRAM_EDGE, stroke=0.8)
    subplot.addLayer(sLayer)
    subplot.addLayer(LayerXY(Double1d(xlim),Double1d([0,0]), 
                 style=Style(color=Color.red,line=Style.DASHED, stroke=1.0)))
    if (startSubs == 1):
        subplot.baseLayerXY.xaxis.title.visible=0
        subplot.baseLayerXY.xaxis.tick.labelVisible=0
        subplot.baseLayerXY.xaxis.tick.height=0.01    
        subplot.baseLayerXY.yaxis.title.visible=0
        subplot.baseLayerXY.yaxis.tick.labelVisible=0
        subplot.baseLayerXY.yaxis.tick.height=0.01
    else:
        startSubs = 1
        subplot.baseLayerXY.xaxis.title.text='Wavelength (um)'
        subplot.baseLayerXY.yaxis.title.text='Flux (Jy)'
    subplot.baseLayerXY.xaxis.tick.minorNumber=0
    subplot.baseLayerXY.yaxis.tick.minorNumber=0    
    myPlot.addSubPlot(subplot)
    subplot.getLayer(0).yrange = ylim
    subplot.getLayer(0).xrange = xlim


##### Save ####


# Finally, save the figure to PDF
myPlot.saveAsPDF(os.path.join(workdir,
         map.meta["object"].value.upper()+"_contour_spectra.pdf"))
