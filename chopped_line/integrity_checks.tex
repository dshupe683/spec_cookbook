
 
\date{\today}
\title{PACS Spectroscopy Recipe: Integrity Checks for Chopped Data}

\documentclass[letterpaper,12pt, ]{article}
\usepackage[usenames,dvipsnames]{color}
\usepackage{tikz}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{wrapfig}
\usepackage{subcaption}
\usepackage{hyperref}

\textwidth=6.5in
\textheight=9.5in
\topmargin=-0.75in
\oddsidemargin=0.0in
\evensidemargin=0.0in

\pagestyle{myheadings}

\definecolor{light-gray}{gray}{0.90}
\definecolor{dark-gray}{gray}{0.4}
\definecolor{dark-blue}{RGB}{25,25,112}

\hypersetup{colorlinks=true,urlcolor=dark-blue}

\lstset{basicstyle=\normalsize\ttfamily,
        showstringspaces=false,
        basewidth=1.2ex,
        fontadjust=true,
        escapechar=�,
        backgroundcolor=\color{light-gray},
        stringstyle=\color{dark-gray},
        commentstyle=\color{black}
        numbers=left,
        numberstyle=\tiny,
        language=Python}

\newcommand{\roundbox}[1]{%
  \tikz[baseline=-1ex]%
  \node[%
  inner sep=1.5pt,
  draw=black,
  fill=black,
  text=white,
  rounded corners=2.5pt]{#1};}

\newcommand{\boxref}[1]{%
  \begingroup%
  \scriptsize\ttfamily%
  \roundbox{\ref{#1}}%
  \endgroup%
}

\newcount\mymark
\makeatletter
\def\mycoderef#1{%
  \global\advance\mymark by 1%
  \protected@write \@auxout {}{\string \newlabel {#1}{{\the\mymark}{}}}%
  \makebox[0pt][r]{{\scriptsize\roundbox{\the\mymark}~}}%
}
\makeatother



\begin{document}

\maketitle


\section{Introduction}\label{sec:intro}

This recipe covers the steps needed to process PACS line spectroscopy of sources observed with chopping.
All the steps for observing the [OI] 63 $\mu$m line in T Tauri are covered from beginning to end. 

In this mode, the detectors are chopped on and off the source. The telescope is moved between two nod
positions to account for any linear gradients in the background. The chopping corrects for responsivity changes
as are caused by glitches.
%%% Add here a figure showing the chopping and nodding.

%%% Need to say what flux range this is applicable.
%%% What is T Tauri (Class 2 Protostar), that it is extended.
%%% Show the image from PACS 70 and 160 microns.
%%% What is the goal we are trying to achieve with this observation.


Before embarking on the steps in this recipe, the reader must should have an installation of the 
\href{http://herschel.esac.esa.int/HIPE_download.shtml}{Herschel Interactive Processing Environment (HIPE)} 
available and should be familiar with the \href{http://herschel.esac.esa.int/hcss-doc-12.0/load/quickstart/html/quickstart.html}{HIPE Quickstart Guide}.

A companion script contains the all the processing commands, so it is not necessary to copy and paste from this document into HIPE.

%%% Need to identify the routine checks to do no matter what.


\section{Integrity check of Level 0 data}\label{sec:integrity}
Before starting the reprocessing, you should perform some checks of the integrity of the Level-0 data. 
First is to plot the grating position as a function of time. The check here is to make sure that the grating
moved. The grating is set to a calibration position for a short time, then is moved to the position needed
for the wavelength observation, scanning back and forth over the wavelength range. At times the grating
is moving while data is taken -- these samples will be masked later.
\begin{figure}[ht]
\center\scalebox{0.7}{\includegraphics{figures/ttau_grating.pdf}}
\caption{Grating position vs. time. Check this plot to ensure the grating moved properly.}
\label{fig:grating}
\end{figure}

Next, the chopper position is plotted (Figure \ref{fig:chopper}). For the calibration block, the chopper position ranges widely between two
internal blackbodies. Then a smaller chop is used for the first nod, with a pause, followed by the second nod.
\begin{figure}[ht]
    \begin{subfigure}[b]{0.5\textwidth}
        \includegraphics[width=\textwidth]{figures/ttau_chopper_full.pdf}
        \caption{Chopper position}\label{fig:chopfull}
    \end{subfigure}
    \begin{subfigure}[b]{0.5\textwidth}
        \includegraphics[width=1.04\textwidth]{figures/ttau_chopper_zoomed.pdf}
        \caption{Zoomed to individual points}\label{fig:chopzoom}
    \end{subfigure}
\caption{Chopper position as a function of time. Note that some data must be masked when the chopper wasn't settled.}\label{fig:chopper}
\end{figure}
Note that the chopper is still moving during some of the data-taking (Figure \ref{fig:chopzoom}). These points will be masked later.

Finally, the raw spectrum is plotted in Figure \ref{fig:rawsignal}. The effects of glitches are the most striking feature of the plot. The zoomed-in
portion shows a scan over the line, modulated by chopping and affected by several glitches. The effect of the strongest glitch manifests itself
as a drop in responsively in the succeeding samples.
\begin{figure}[ht]
    \begin{subfigure}[b]{0.5\textwidth}
        \includegraphics[width=\textwidth]{figures/rawsignal.pdf}
        \caption{Raw signal for observation}\label{fig:raw_full}
    \end{subfigure}
    \begin{subfigure}[b]{0.5\textwidth}
        \includegraphics[width=1.0\textwidth]{figures/rawsignal_zoomed.pdf}
        \caption{Zoomed to a scan over the line}\label{fig:raw_zoom}
    \end{subfigure}
\caption{Raw signal as a function of time, showing glitches in the full plot, and and a scan over the line in the second plot.
The modulation of the signal is  caused by chopping.
The effect of the glitch just after t=466 s is seen in reduced responsively thereafter.}\label{fig:rawsignal}
\end{figure}

\section{Footprint check}\label{footprint}

The next sections process the Level 0 (raw) data to Level 0.5 and to Level 1. First, the multithreading is
set by
\lstinputlisting[language=Python,numbers=left,firstline=150, lastline=151, firstnumber=151]{ttau_chopped.py}
where a value of 0 is the default and will use as many threads as possible. If you wish you can set the values
to different values (less than the total number of cores on your computer).

Then the processing continues to the point where coordinates have been assigned to all data points using
\lstinline$specAssignRaDec$. The footprint of the observation is displayed in Figure \ref{fig:footprint},
which shows the positions of all the spaxels together with the telescope boresight and the target
position. The boresight moves between the two nod positions. 
\begin{figure}[ht]
\center\includegraphics[width=0.8\textwidth]{figures/ttau_newfootprint.pdf}
\caption{Footprints of the PACS spaxels for the ON Source (red) and OFF source (green), and the telescope boresight (gray),
together with the target position (red filled square). }
\label{fig:footprint}
\end{figure}

The remainder of the processing to Level 0.5 slices the data into the calibration block, the first nod position (Nod B), and the
second nod position (Nod A), then flags the samples where the grating was moving and where the chopper was not settled, as
discussed in Section \ref{sec:integrity}.



\end{document}

