
 

\documentclass[letterpaper,12pt ]{article}
\usepackage[usenames,dvipsnames]{color}
\usepackage{tikz}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{wrapfig}
\usepackage{subcaption}
\usepackage{hyperref}
\usepackage{float}
\usepackage{caption}
\usepackage{booktabs}

\textwidth=6.5in
\textheight=9.5in
\topmargin=-0.75in
\oddsidemargin=0.0in
\evensidemargin=0.0in

\pagestyle{myheadings}

\definecolor{light-gray}{gray}{0.90}
\definecolor{dark-gray}{gray}{0.4}
\definecolor{dark-blue}{RGB}{25,25,112}

\hypersetup{colorlinks=true,urlcolor=dark-blue}

\lstset{basicstyle=\normalsize\ttfamily,
        showstringspaces=false,
        basewidth=1.2ex,
        fontadjust=true,
        escapechar=�,
        backgroundcolor=\color{light-gray},
        stringstyle=\color{dark-gray},
        commentstyle=\color{black}
        numbers=left,
        numberstyle=\tiny,
        language=Python,
        inputencoding=utf8/latin1,
        extendedchars=\true}
\lstset{literate={�}{{u}}1 {?}{{$^2$}}1}


\newcommand{\roundbox}[1]{%
  \tikz[baseline=-1ex]%
  \node[%
  inner sep=1.5pt,
  draw=black,
  fill=black,
  text=white,
  rounded corners=2.5pt]{#1};}

\newcommand{\boxref}[1]{%
  \begingroup%
  \scriptsize\ttfamily%
  \roundbox{\ref{#1}}%
  \endgroup%
}

\newcount\mymark
\makeatletter
\def\mycoderef#1{%
  \global\advance\mymark by 1%
  \protected@write \@auxout {}{\string \newlabel {#1}{{\the\mymark}{}}}%
  \makebox[0pt][r]{{\scriptsize\roundbox{\the\mymark}~}}%
}
\makeatother



\begin{document}

\date{\today}
\title{PACS Spectroscopy Recipe: Extracting a Spectrum and Line Parameters from a Point Source Observation}

\maketitle


\section{Introduction}\label{sec:intro}

This recipe covers the extraction of a line intensity and line width, and continuum level, from a
pointed chopped observation of a point source.
This example uses the [OI] 63 $\mu$m line in HD 141569, from observation 1342190376.

Before embarking on the steps in this recipe, the reader must have an installation of the 
\href{http://herschel.esac.esa.int/HIPE_download.shtml}{Herschel Interactive Processing Environment (HIPE)} 
available and should have reviewed the \href{http://herschel.esac.esa.int/hcss-doc-12.0/load/quickstart/html/quickstart.html}{HIPE Quickstart Guide}.
We also assume familiarity with the preceding recipes  ``Identifying and Accessing Observations'', and
``Inspecting a Pointed Chopped Line Spectroscopy Observation''.

In this recipe we start from the Level-2 products in the Herschel Science Archive (HSA) and we also indicate the
equivalent products and steps in the ChopNodLineScan.py interactive pipeline (ipipe) script in HIPE.

A companion script contains the all the processing commands, so it is not necessary to copy and paste from this document into HIPE.

\section{Locating spectral cubes in the observation context}

\begin{figure}
  \center\includegraphics[scale=0.65,keepaspectratio=true]{figures/hd141569_level2_contents.pdf}
  \caption{The contents of Level-2 for the observation context 1342190376, as displayed in the left panel of the Observation Context Viewer
  in HIPE. HPS3DB is the blue spectral without wavelength rebinning, with separate cubes for the nod positions A and B.
   HPS3DRB is the rebinned blue cube; the wavelength rebinning has allowed the two nods to be averaged into a single cube. The other
   Level-2 products are not needed for this recipe.}
  \label{fig:level2}
\end{figure}

We start with the cubes in the Level-2 context. Figure \ref{fig:level2} shows
 the contents as displayed in the Editor area
in HIPE.  Here we will concern ourselves only with the ``blue'' cubes. In
order of increasing processing level, these are: 
\begin{itemize}
  \item{{\bf HPS3DB:}  Flat-fielded cubes, one per nod position, with no wavelength rebinning. This product is equivalent to
    the {\bf slicedCubes} variable in the ipipe script after the spectral-flat-fielding step. For this example each cube has
    dimensions [73728,5,5].}
    \item{{\bf HPS3DRB:} Wavelength-rebinned cube with both nod positions combined. For a single-pointing observation such as this
    one, this is the best cube to use for spectrum extraction. Its equivalent in the ipipe script is {\bf slicedFinalCubes}. For this
    observation the cube dimensions are [56,5,5].}
  \item{{\bf HPS3DPB:} The HPS3DRB cube projected onto a celestial grid. Not of interest for this single-pointing case. Its
   equivalent in the ipipe script is {\bf slicedProjectedCubes}.}
\end{itemize}

\section{A first extraction from the rebinned cube}

The next steps of the companion script extract the spectrum from the rebinned HPS3DRB cube. There are three spectra
that are extracted by this line:
\lstinputlisting[language=Python, numbers=left, label=lst:myhsa, firstline=24, lastline=25, firstnumber=24]{pointsrc_line_extraction.py}
The first spectrum, {\bf c1}, is the central spectrum with a point-source correction applied, to correct for about 30\% of the flux for a perfectly
centered point source falling outside the central spaxel. The second spectrum, {\bf c9}, is the ``Central 9'' spectrum, summing up the central 9
spaxels, with a point-source correction applied for flux falling outside of these spaxels. The third spectrum, {\bf c129}, is the central spaxel with
a point-source correction and an additional correction for the expected ratio of the central 9 (or 3x3) spaxels to the central spaxel.
This ``3x3 correction'' compares the ratio of the flux in the central spaxel to
the 3x3 ``super-spaxel'' to the ratio for a perfectly aligned point source, and is meant to correct for flux losses due to mis-centering
of a point source. The task outputs to the Console
\begin{lstlisting}
BAND B3A,   wavelength range: [ 62.95, 63.45],   (median)correction:  1.319,   Rel. Error on (mean)corr.:  5.11\%,  RMS(raw correction):  0.382 ( 38.2\%)
\end{lstlisting}
The ratio of ratios is the correction and is displayed
as a function of wavelength in Figure \ref{fig:3x3correction}.
\begin{figure}
\center\includegraphics[width=0.8\textwidth]{figures/hd141569_correctioncurves.pdf}
\caption{{\bf (Note we would like to heavily revise this plot.)} Diagnostic plot of the 3x3 correction computed within the extractCentralSpectrum task. The black curve is obtained by comparing
the ratio of the signal in the central spaxel to the signal in the 3x3 central spaxels, and adjusting to that of a perfectly centered
point source. The blue line is the median over all wavelengths and is used in this case. The green line can be ignored. 
The correction is not to be trusted in this case because the
continuum is below 10 Jy and the standard deviation of the correction is large. }
\label{fig:3x3correction}
\end{figure}

The ``3x3 correction'' is reliable only for point sources whose continuum levels are at least 10 Jy. While our target is a point source, the
continuum level is not bright enough to trust the correction. The RMS of the raw correction derived from every wavelength is 38\% which
is also an indication that the correction is not trustworthy in this case. In general the correction factor should lie between 1.0 and 1.2 to 1.3 in
most cases. (Note that for bright sources, the magnitude of the ``3x3 correction'' can be used to assess whether a source is extended--
though it is preferable to examine imaging data as presented in the ``Inspecting a Pointed Chopped Line Spectroscopy Observation'' companion recipe.) 

The resulting corrected spectra are shown in Figure \ref{fig:extractions}. 
\begin{figure}
        \includegraphics[width=1.0\textwidth]{figures/hd141569_firstextraction.pdf}
\caption{Extracted spectra for HD 141569 with various corrections applied. The uncorrected spectrum (blue) is simply the
spectrum of the central spaxel from the reprocessed cube. For comparison, the uncorrected spectrum from the v11.1.0
cube from the archive (teal) shows a different wavelength sampling in the line but otherwise not much difference. Applying just the aperture correction (green) gives about
the right continuum flux for this object compared to broad-band measurements. Applying both the aperture correction
and the 3x3 correction (brown) makes the spectrum too bright. }
\label{fig:extractions}
\end{figure}
This plot is shown by the extractCentralSpectrum task when the verbose flag is set. Note that 
the ``Central 9'' spectrum ({\bf c9}) is substantially noisier in the continuum than the
spectra obtained from the central spectrum itself ({\bf c1}). The 3x3-corrected spectrum 
({\bf c129}) has the highest flux in the continuum and the line peak, due to the multiplication by 1.32 that
was calculated as the 3x3 correction. Since the continuum
of our source is not near the 10 Jy threshold, the 3x3 correction is not reliable in our case. Note that it is possible
to be misled by the continuum of {\bf c9} matching the continuum of {\bf c129} -- this agreement is by construction
and does not by itself indicate the that the 3x3 correction can be trusted.


\section{Fitting for the line flux, width, and continuum}

Fitting spectra interactively using the Spectrum Explorer and Spectrum Fitter is explained in the 
Data Analysis Guide (DAG) and the PACS Spectroscopy Data Reduction Guide (PDRG). In
this recipe we include a function to fit a single bright line and a simple continuum, using the
command-line form of Spectrum Fitter. In verbose mode, the fitter will show a fitted spectrum with
the model (Figure \ref{fig:firstfit} and a model-subtracted residual (not shown here).
\begin{figure}
  \includegraphics[width=\textwidth]{figures/hd141569_firstfitting.pdf}
  \caption{Plot from fitting the pipeline product.}
  \label{fig:firstfit}
\end{figure}

The units that are obtained for the integrated line intensity from the Spectrum Fitter are in Jy $\mu$m. To convert to
more widely-used SI units, we must apply the relation $\delta\nu F_{\nu} = \delta\lambda F_{\lambda}$. The fitting function
reads the units of the Spectrum Fitter output and makes the appropriate conversion to express the line intensity
in W m$^{-2}$. The output of the fitting function for the first extracted spectra is shown below.


\begin{lstlisting}
Line center =     63.183 +/- 0.000369 um [1 um = 1.0E-6 m]
Line peak =           11 +/-  0.383 Jy [1 Jy = 1.0E-26 W/m2/Hz]
Line FWHM =       0.0218 +/- 0.000892 um [1 um = 1.0E-6 m]
Continuum =         3.44 +/- 0.0581 Jy [1 Jy = 1.0E-26 W/m2/Hz]
Line integral = 1.92e-16 +/- 1.03e-17 W/m2
\end{lstlisting}

In Table \ref{tab:firstlinefit} we summarize the results for the three extracted spectra.
\begin{table*}
\begin{tabular}{lccc}
\toprule
      &   \multicolumn{3}{c}{Spectrum} \\
    Quantity  & c1 & c9 &  c129 \\
      \midrule
      Line center [$\mu$m]  & $63.183 \pm 0.0004$ & $63.184 \pm 0.0009$ & $63.183 \pm 0.0004$ \\
      Line peak [Jy]  & $11.0 \pm  0.4$ & $12.1 \pm  0.9$ & $14.5 \pm  0.5$ \\
      Line FWHM [$\mu$m]  &  $0.0218 \pm 0.0009$ &   $0.0244 \pm 0.002$ & $0.0218 \pm 0.0009$ \\
      Continuum [Jy] & $3.44 \pm 0.06$ & $4.54 \pm 0.14$ & $4.53 \pm 0.08$ \\
      Line integral  [$10^{-18}$ W m$^{-2}$]& $192 \pm 10$ & $238 \pm 26$ & $253 \pm 14$\\
      \bottomrule
\end{tabular}
\caption{Fits to the OI 63 $\mu$m line for the three extracted spectra from the SPG 11.1.0 cube.}
\label{tab:firstlinefit}
\end{table*}
We see that the fitted values for the c9 spectrum have greater uncertainties than the other two that
are based on the central spaxel. 

%BAND B3A,    wavelength range: [ 62.95, 63.45],    (median)correction:  1.319,    Rel. Error on (mean)corr.:    5.11%,    RMS(raw correction):  0.382 ( 38.2%)
%Line center =     63.183 +/- 0.000369 um [1 um = 1.0E-6 m]
%Line peak =           11 +/-  0.383 Jy [1 Jy = 1.0E-26 W/m2/Hz]
%Line FWHM =       0.0218 +/- 0.000892 um [1 um = 1.0E-6 m]
%Continuum =         3.44 +/- 0.0581 Jy [1 Jy = 1.0E-26 W/m2/Hz]
%Line integral = 1.92e-16 +/- 1.03e-17 W/m2
%Line center =     63.184 +/- 0.000851 um [1 um = 1.0E-6 m]
%Line peak =         12.1 +/-  0.866 Jy [1 Jy = 1.0E-26 W/m2/Hz]
%Line FWHM =       0.0244 +/- 0.00204 um [1 um = 1.0E-6 m]
%Continuum =         4.54 +/-   0.14 Jy [1 Jy = 1.0E-26 W/m2/Hz]
%Line integral = 2.38e-16 +/- 2.62e-17 W/m2
%Line center =     63.183 +/- 0.000369 um [1 um = 1.0E-6 m]
%Line peak =         14.5 +/-  0.505 Jy [1 Jy = 1.0E-26 W/m2/Hz]
%Line FWHM =       0.0218 +/- 0.000892 um [1 um = 1.0E-6 m]
%Continuum =         4.53 +/- 0.0766 Jy [1 Jy = 1.0E-26 W/m2/Hz]
%Line integral = 2.53e-16 +/- 1.36e-17 W/m2


\section{Variation: Changing the wavelength grid}

The parameters upsample and oversample define how the wavelength rebinning is done, as described
in PDRG section 6.2. Oversample
sets the bin size to be spectral resolution divided by oversample. For Nyquist sampling, oversample is set to 2.
The upsample parameter controls overlapping of the wavelength bins. For upsample=1, each wavelength bin is independent. 
For an upsample value of 2, each wavelength bin starts in the middle of its adjacent bin. For upsample values greater than 1, points
will be averaged into more than one bin, appearing in upsample bins, and the spectrum points will be correlated, and noise
statistics will be affected.

The rebinned cube from the archive was generated with oversample=2 and upsample=2. The ipipe script for chopped line
spectroscopy defaults to oversample=2 and upsample=4. Our comparison script contains steps for regridding the
HPS3DB cube in the same way the pipeline does it. The extraction is shown in Figure \ref{fig:extractupsample4}
\begin{figure}
  \includegraphics[width=\textwidth]{figures/hd141569_extraction_upsample4.pdf}
  \caption{Extracted spectra from ipipe script with upsample=4, oversample=2.}
  \label{fig:extractupsample4}
\end{figure}
and the fitted spectrum in Figure \ref{fig:fitupsample4}.
\begin{figure}
  \includegraphics[width=\textwidth]{figures/hd141569_fitting_upsample4.pdf}
  \caption{Plot from fitting the spectrum from the regridded data.}
  \label{fig:fitupsample4}
\end{figure}
Note that the line profile is smoother.

The values from fitting the lines are shown in Table \ref{tab:upsample4}.
\begin{table*}
\begin{tabular}{lccc}
\toprule
      &   \multicolumn{3}{c}{Spectrum} \\
    Quantity  & c1 & c9 &  c129 \\
      \midrule
      Line center [$\mu$m]  & $63.183 \pm 0.0002$ & $63.185 \pm 0.0005$ & $63.183 \pm 0.0002$ \\
      Line peak [Jy]  & $11.1 \pm  0.2$ & $12.1 \pm  0.5$ & $14.9 \pm  0.3$ \\
      Line FWHM [$\mu$m]  &  $0.0215 \pm 0.0004$ &   $0.0243 \pm 0.001$ & $0.0215 \pm 0.0004$ \\
      Continuum [Jy] & $3.43 \pm 0.03$ & $4.57 \pm 0.08$ & $4.61 \pm 0.04$ \\
      Line integral  [$10^{-18}$ W m$^{-2}$]& $192 \pm ~5$ & $236 \pm 15$ & $258 \pm ~74$\\
      \bottomrule
\end{tabular}
\caption{Fits to the OI 63 $\mu$m line for the three extracted spectra from the cube rebinned with upsample=4.}
\label{tab:upsample4}
\end{table*}
The results of using upsample=4 are changed very little from fitting the upsample=1 case, with the exception that
the uncertainties decrease by a factor of 2 for upsample=4. This decrease is expected since we have four times
as many points falling into each wavelength bin, so that uncertainties ought to decrease by $\sqrt{\rm upsample}$.

\section{Variation: Estimating flux loss due to pointing misalignment}

As noted in the recipe ``Inspecting a Pointed Chopped Line Spectroscopy Observation'', overlays of the spaxels
on various images do appear to indicate a slight mis-alignment of the central spaxel with the target. Assuming
the spaxel coordinates are correct, we can estimate by how much the misalignment affects the flux density in
the central spaxel, using the semi-extended source correction tool. With the true coordinates in variables
``ra\_true'' and ``dec\_true'', the correction and corrected spectrum are computed by:
\lstinputlisting[language=Python, numbers=left, firstline=109, lastline=118, firstnumber=109]{pointsrc_line_extraction.py}
The correction in this case is to divide by 0.938. {\bf(Note: the calculation should be checked by Elena -- the results
vary with the sign of the offsets so are not symmetric.)}



\end{document}

