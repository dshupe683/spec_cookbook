obsid = 1342190376

#################################################################################################################
# Classic retrieval using getObservation and saveObservation
#################################################################################################################


# If you already have the data stored locally in a pool with saveObservation, 
# the data is read from that pool.
#
# If you do not have the data stored locally, change useHsa to 1.
# You will in this case retrieve the data from the HSA and then make a local 
# copy in a pool (i.e. on disc). 
#
# About getObservation / saveObservation:   
# 
#    The poolName & poolLocation (directory names) parameters in getObservation 
#    are optional. 
# 
#    poolLocation: The poolLocation is the path where your data are stored. By 
#                  default that is $HOME/.hcss/lstore. If that is where you 
#                  had put your Herschel data (as recommended) you need make 
#                  no changes below. It is specified in a  property and can be 
#                  checked with
#                  > print Configuration.getProperty("hcss.ia.pal.pool.lstore.dir")
# 
#    poolName: This is an optional parameter and is the immediate directory containing 
#              your data. By default the poolName is the obsid.
#    
#   If your data are instead in /Users/bigdisc/Herschel/NGC111, or you wish to save them there rather 
#   than the default location, then poolLocation="/Users/bigdisc/Herschel" and 
#   poolName="NGC111". In this case you need to change these parameters, which are None in the 
#   example here 
# 
useHsa = 1
obs    = getObservation(obsid, verbose=True, useHsa=useHsa, \
             poolLocation=None, poolName=None)

# Set compression on in local pools
Configuration.setProperty("hcss.ia.pal.pool.lstore.compress","True")
Configuration.save(["hcss.ia.pal.pool.lstore.compress"])

# Edit poolDir as desired
#poolDir = str(Configuration.getWorkDir()) + '/hd141569/'
poolDir = Configuration.getProperty('hcss.ia.pal.pool.lstore.dir')
# Save products in the background so you can keep working
bg("saveObservation(obs, poolLocation='"+poolDir+"')")
# Note: without compression, pool size is 2.0 GB.
#    with compression, pool size is 550 MB.

#################################################################################################################
# Alternative: Retrieve the observation 'on-demand' using MyHSA
#################################################################################################################

# When these two items are set then useHsa=1 is unnecessary,
# and any product you access will also
# be saved into the MyHSA Collection.
MyHSAPool.getInstance().setConnection(MyHSAPool.ONCACHED)
Configuration.setProperty("hcss.ia.pal.pool.hsa.on_demand","True")

obs = getObservation(obsid)



