
 
\date{\today}
\title{PACS Spectroscopy Recipe: Chopped Line Spectroscopy of Point and Compact Sources}

\documentclass[letterpaper,12pt, ]{article}
\usepackage[usenames,dvipsnames]{color}
\usepackage{tikz}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{wrapfig}
\usepackage{subcaption}
\usepackage{hyperref}

\textwidth=6.5in
\textheight=9.5in
\topmargin=-0.75in
\oddsidemargin=0.0in
\evensidemargin=0.0in

\pagestyle{myheadings}

\definecolor{light-gray}{gray}{0.90}
\definecolor{dark-gray}{gray}{0.4}
\definecolor{dark-blue}{RGB}{25,25,112}

\hypersetup{colorlinks=true,urlcolor=dark-blue}

\lstset{basicstyle=\normalsize\ttfamily,
        showstringspaces=false,
        basewidth=1.2ex,
        fontadjust=true,
        escapechar=�,
        backgroundcolor=\color{light-gray},
        stringstyle=\color{dark-gray},
        commentstyle=\color{black}
        numbers=left,
        numberstyle=\tiny,
        language=Python}

\newcommand{\roundbox}[1]{%
  \tikz[baseline=-1ex]%
  \node[%
  inner sep=1.5pt,
  draw=black,
  fill=black,
  text=white,
  rounded corners=2.5pt]{#1};}

\newcommand{\boxref}[1]{%
  \begingroup%
  \scriptsize\ttfamily%
  \roundbox{\ref{#1}}%
  \endgroup%
}

\newcount\mymark
\makeatletter
\def\mycoderef#1{%
  \global\advance\mymark by 1%
  \protected@write \@auxout {}{\string \newlabel {#1}{{\the\mymark}{}}}%
  \makebox[0pt][r]{{\scriptsize\roundbox{\the\mymark}~}}%
}
\makeatother



\begin{document}

\maketitle


\section{Introduction}\label{sec:intro}

This recipe covers the steps needed to process PACS line spectroscopy of sources observed with chopping.
All the steps for observing the [OI] 63 $\mu$m line in T Tauri are covered from beginning to end. 

In this mode, the detectors are chopped on and off the source. The telescope is moved between two nod
positions to account for any linear gradients in the background. The chopping corrects for responsivity changes
as are caused by glitches.
%%% Add here a figure showing the chopping and nodding.

%%% Need to say what flux range this is applicable.
%%% What is T Tauri (Class 2 Protostar), that it is extended.
%%% Show the image from PACS 70 and 160 microns.
%%% What is the goal we are trying to achieve with this observation.


Before embarking on the steps in this recipe, the reader must should have an installation of the 
\href{http://herschel.esac.esa.int/HIPE_download.shtml}{Herschel Interactive Processing Environment (HIPE)} 
available and should be familiar with the \href{http://herschel.esac.esa.int/hcss-doc-12.0/load/quickstart/html/quickstart.html}{HIPE Quickstart Guide}.

A companion script contains the all the processing commands, so it is not necessary to copy and paste from this document into HIPE.

%%% Need to identify the routine checks to do no matter what.

\section{Identifying the observation in the Herschel Science Archive}\label{sec:id-archive}

Each observation performed by Herschel is identified by an Observation ID or obsid. You must first identify the
obsid for the data you would like to look at. Here are three methods you can use.

\subsection{Using the Herschel Science Archive User Interface}

The Herschel Science Archive User Interface (HUI) is a Java applet that enables direct access to the data, as well
as convenient display of ``browse products'' or thumbnails. The HUI can be launched from a HIPE session (following
\href{http://herschel.esac.esa.int/hcss-doc-12.0/load/quickstart/html/qs_open_hsa.html}{the instructions in the Quickstart Guide})
or \href{http://herschel.esac.esa.int/Science_Archive.shtml}{launching from a web browser}. From within the HUI, enter
T Tauri as the target name and query the archive. You will see a number of photometry and spectroscopy observations.
For this recipe the PACS Line Spectroscopy observation of the O I 63$\mu$m line is selected, with obsid 1342190353.
Resist the temptation to download the tar file of all the data! The script for this recipe will download only those parts of
the observation that are needed and can save a great deal of downloading time.

\subsection{Using the Herschel Data Search at IRSA}

The Infrared Science Archive (IRSA) provides a web interface to search the archive at 
\href{http://irsa.ipac.caltech.edu/applications/Herschel/}{http://irsa.ipac.caltech.edu/applications/Herschel/}.
Type ``T Tauri'' into the Coordinate/Object box and set the search radius to 1 arcmin. A new browser tab
will appear with a table much like that shown in the HUI. Resist the temptation to click on the Observation ID
link! It will begin downloading a tarfile of all data in the observation.

\subsection{Using the Herschel Observation Log at Vizier}

The Herschel Observation Log may be search at Vizier at either the \href{http://vizier.u-strasbg.fr/viz-bin/VizieR?-source=VI\%2F139}{Strasbourg site}
or the \href{http://vizier.cfa.harvard.edu/viz-bin/VizieR?-source=VI\%2F139}{US mirror site}. Enter ``T Tauri'' as the object name
and a radius of 1 arcmin. The resulting table shows one PACS Line Spectroscopy observation with obsid = 1342190353.


\section{Accessing the data within HIPE}\label{sec:access}

The remainder of this recipe makes use of the companion script \lstinline$ttau_chopped.py$. Only selected lines from the
script are shown in this document. They are marked by line numbers to make them easy to find in the HIPE Editor view.

In this recipe you will not need to download or access every type of data associated with the observation. 
These script lines ensure that the archive is online and that as data products are accessed,
they will be stored on your machine.
% Note can add "numbers=left" for line numbering -- conflicts with mycoderef.
\lstinputlisting[language=Python, numbers=left, label=lst:myhsa, firstline=5, lastline=6, firstnumber=5]{ttau_chopped.py}

The observation context  is loaded into HIPE with:
\lstinputlisting[language=Python, numbers=left, firstline=7, lastline=8, firstnumber=7]{ttau_chopped.py}

If you are not currently logged into the HSA, you may see an error like 
\begin{lstlisting}
herschel.ia.task.TaskException: Error processing getObservation task: 
  Not logged in (no credentials provided).
\end{lstlisting}
In this case, you should check the bottom status-bar of HIPE for the HSA log-in status, and if necessary, to click 
on the icon to log in with your Herschel username and password.

In preparation for the next sections, you should edit these next lines to specify the working directory and
whether you will work with ``blue'' or ``red'' data:
\lstinputlisting[language=Python, numbers=left, firstline=10, lastline=12, firstnumber=10]{ttau_chopped.py}

\section{Exploring the data in HIPE}\label{sec:explore}

The next steps explore the higher-level products.

\subsection{Text summary of the observation}\label{sec:textsummary}
A text listing of the observation is produced with
\lstinputlisting[language=Python, numbers=left, firstline=22, lastline=22, firstnumber=22]{ttau_chopped.py}
which yields detailed information about the observation. Check the output for the mode (Pointed, Chop/Nod for this recipe),
and the prime observing blocks (Camera=Blue, Band=B3A, O I 3P1-3P2, 63.18 micron). Check that you have set
the value of the ``camera'' variable correctly in the previous section.

\subsection{Source overlay on an image}
Next, a 2MASS J-band image is retrieved, and read into a variable ``image''. Then an overlay of the footprint is displayed using
\lstinputlisting[language=Python,numbers=left,firstline=52, lastline=52, firstnumber=52]{ttau_chopped.py}
\lstinputlisting[language=Python,numbers=left,firstline=55, lastline=58, firstnumber=55]{ttau_chopped.py}
with the result shown in Figure \ref{fig:overlay}.
\begin{figure}
\center\includegraphics[width=1.0\textwidth]{figures/ttau_overlay_2mass_pacs.pdf}
\caption{Top left: Overlay of spaxels (spatial pixels of the spectral cube) on a 2MASS J-band image. The spaxels are numbered as (row, column). Top right: spectrum at the central spaxel. The line at 63.32 $\mu$m is from ortho-water. Lower left: Overlay on 70 $\mu$m image. Lower right: Overlay on 160 $\mu$m image.}\label{fig:overlay}
\end{figure}
Similar methods are used to retrieve a PACS image of T Tau from the archive and to overlay the spaxel locations.


\subsection{Versions of software and calibration}
Check the version of the pipeline and of the calibration tree, and compare it to the current HIPE version
and the calibration tree that HIPE has downloaded. The pipeline (SPG) version and the calibration tree
version are displayed by the \lstinline$obsSummary$ function in section \ref{sec:textsummary}. They are also available in
the metadata using
\lstinputlisting[language=Python, numbers=left, firstline=76, lastline=77, firstnumber=76]{ttau_chopped.py}
The HIPE version you are using can be checked from top-level menu Help:About. Both the software
and calibration versions can be checked with 
\lstinputlisting[language=Python,numbers=left,firstline=79, lastline=81, firstnumber=79]{ttau_chopped.py}

Finally, plot all the spectra in 5x5 format (Figure \ref{fig:5x5}) with
\lstinputlisting[language=Python,numbers=left,firstline=84, lastline=84, firstnumber=84]{ttau_chopped.py}
\begin{figure}[t]
\center\includegraphics[width=\textwidth]{figures/ttau_5x5.pdf}
\caption{5x5 plot of all spaxels in the O I 63$\mu$m line of T Tauri. Flux density is scaled from -5 to 400 Jy/pixel
and wavelength from 63.0 to 63.4 $\mu$m.  }\label{fig:5x5}
\end{figure}
Here a {\it spaxel} refers to a spatial pixel, i.e. the spatial unit of a spectral cube.

\section{Assessing whether reprocessing is needed}\label{sec:assess-reprocessing}
%%% Need to say here that the Level 2 data could look fine but can hide some problems.
%%% The issue is that the chop-off has been subtracted, and the nods have been combined.
%%% Maybe you've chopped onto a source. Can we show the chopper throw?

%%% It's a weak justification to simply say "the calibration version changed and the s/w version changed."

%%% Also note that the masking of lines can be done better by the user, in the spectral flat-fielding stage.

In general, the calibration version and the software version will usually be advanced compared to the versions
available in the archive. Since the spectroscopy calibration is still being refined, it is advisable to reprocess
the data starting from Level 0.

In the example used in this recipe, the pipeline processing version is v10.3.0 and the calibration version is 48.
The HIPE version (official user release) is 11.0.2938 or User Release v11.0.1. The calibration version is 56.
Therefore, we reprocess the data.

\section{Integrity check of Level 0 data}\label{sec:integrity}
Before starting the reprocessing, you should perform some checks of the integrity of the Level-0 data. 
First is to plot the grating position as a function of time. The check here is to make sure that the grating
moved. The grating is set to a calibration position for a short time, then is moved to the position needed
for the wavelength observation, scanning back and forth over the wavelength range. At times the grating
is moving while data is taken -- these samples will be masked later.
\begin{figure}[ht]
\center\scalebox{0.7}{\includegraphics{figures/ttau_grating.pdf}}
\caption{Grating position vs. time. Check this plot to ensure the grating moved properly.}
\label{fig:grating}
\end{figure}

Next, the chopper position is plotted (Figure \ref{fig:chopper}). For the calibration block, the chopper position ranges widely between two
internal blackbodies. Then a smaller chop is used for the first nod, with a pause, followed by the second nod.
\begin{figure}[ht]
    \begin{subfigure}[b]{0.5\textwidth}
        \includegraphics[width=\textwidth]{figures/ttau_chopper_full.pdf}
        \caption{Chopper position}\label{fig:chopfull}
    \end{subfigure}
    \begin{subfigure}[b]{0.5\textwidth}
        \includegraphics[width=1.04\textwidth]{figures/ttau_chopper_zoomed.pdf}
        \caption{Zoomed to individual points}\label{fig:chopzoom}
    \end{subfigure}
\caption{Chopper position as a function of time. Note that some data must be masked when the chopper wasn't settled.}\label{fig:chopper}
\end{figure}
Note that the chopper is still moving during some of the data-taking (Figure \ref{fig:chopzoom}). These points will be masked later.

Finally, the raw spectrum is plotted in Figure \ref{fig:rawsignal}. The effects of glitches are the most striking feature of the plot. The zoomed-in
portion shows a scan over the line, modulated by chopping and affected by several glitches. The effect of the strongest glitch manifests itself
as a drop in responsively in the succeeding samples.
\begin{figure}[ht]
    \begin{subfigure}[b]{0.5\textwidth}
        \includegraphics[width=\textwidth]{figures/rawsignal.pdf}
        \caption{Raw signal for observation}\label{fig:raw_full}
    \end{subfigure}
    \begin{subfigure}[b]{0.5\textwidth}
        \includegraphics[width=1.0\textwidth]{figures/rawsignal_zoomed.pdf}
        \caption{Zoomed to a scan over the line}\label{fig:raw_zoom}
    \end{subfigure}
\caption{Raw signal as a function of time, showing glitches in the full plot, and and a scan over the line in the second plot.
The modulation of the signal is  caused by chopping.
The effect of the glitch just after t=466 s is seen in reduced responsively thereafter.}\label{fig:rawsignal}
\end{figure}

\section{Footprint check}\label{footprint}

The next sections process the Level 0 (raw) data to Level 0.5 and to Level 1. First, the multithreading is
set by
\lstinputlisting[language=Python,numbers=left,firstline=150, lastline=151, firstnumber=151]{ttau_chopped.py}
where a value of 0 is the default and will use as many threads as possible. If you wish you can set the values
to different values (less than the total number of cores on your computer).

Then the processing continues to the point where coordinates have been assigned to all data points using
\lstinline$specAssignRaDec$. The footprint of the observation is displayed in Figure \ref{fig:footprint},
which shows the positions of all the spaxels together with the telescope boresight and the target
position. The boresight moves between the two nod positions. 
\begin{figure}[ht]
\center\includegraphics[width=0.8\textwidth]{figures/ttau_newfootprint.pdf}
\caption{Footprints of the PACS spaxels for the ON Source (red) and OFF source (green), and the telescope boresight (gray),
together with the target position (red filled square). }
\label{fig:footprint}
\end{figure}

The remainder of the processing to Level 0.5 slices the data into the calibration block, the first nod position (Nod B), and the
second nod position (Nod A), then flags the samples where the grating was moving and where the chopper was not settled, as
discussed in Section \ref{sec:integrity}.

\section{Interactive: Inspecting masks and glitches}\label{sec:glitches}

The first steps in processing from Level 0.5 to Level 1 involve the masking of glitches and noisy detectors. Glitches are
flagged by \lstinline$specFlagGlitchFramesQTest$ which creates a mask called GLITCH. The masks for glitches,
grating moves, and unsettled (unclean) chopping can be inspected with the MaskViewer:
\lstinputlisting[language=Python,numbers=left,firstline=235, lastline=236, firstnumber=168]{ttau_chopped.py}
At this stage you may wish to view the UNCLEANCHOP, GRATMOVE and GLITCH masks using the drop-down
menu in the MaskViewer window (Figure \ref{fig:maskviewer}). 
\begin{figure}
\center\includegraphics[width=0.8\textwidth]{figures/maskviewer_noisy.pdf}
\caption{Display of the GLITCH mask for spaxel 6,0. Red points are masked. This detector shows excess noise in the first 2000 samples.}
\label{fig:maskviewer}
\end{figure}
Select a spaxel and mask in the top half, and zoom in if desired by dragging a box
over the bottom plot.

Some additional lines in the script attempt to find noisy pixels. For this dataset these values are printed for the
spaxel location  (row and column) and the standard deviation of the signal:
\begin{lstlisting}
6 0 0.238372836456
16 12 0.234838903552
\end{lstlisting}
This method is not perfect as it can be thrown off by bright lines. Inspection with the MaskViewer shows that spaxel (16,12)
does not appear especially noisy. However, spaxel (6,0) does appear noisy in the first 2000 samples (Figure \ref{fig:maskviewer}).
 These are masked
with the lines:
\lstinputlisting[language=Python,numbers=left,firstline=253, lastline=256, firstnumber=253]{ttau_chopped.py}

The mask-viewing and noise-finding portions can be repeated for the third slice. In this case no further modifications
of the masks are needed.

\section{Processing to spectral flat-fielding}\label{sec:extraction}

The next portions of the pipeline activate all the masks, calculate the response from the calibration block, subtract
the chopped-off-source from the chopped-on-source signal, apply the response calibration, and form a cube with
\lstinputlisting[language=Python,numbers=left,firstline=267, lastline=267, firstnumber=267]{ttau_chopped.py}

Once the cube is obtained, outliers are flagged and the spectral cube is rebinned into a uniform grid of wavelengths,
resulting in a \lstinline$slicedRebinnedCube$. At this point, you can improve the spectral flat-fielding by specifying
the wavelengths of the lines (printed out with \lstinline$print obs.meta["lines"].string$). For broad lines you may wish
to increase the value of \lstinline$widthmask$. Here we specify the wavelength of the O I line and the additional
line at 63.32 $\mu$m, and use the default width:
\lstinputlisting[language=Python,breaklines=true,numbers=left,firstline=289, lastline=290, firstnumber=289]{ttau_chopped.py}

\section{Science judgment step: Extracting the central spectrum}

The final steps of the companion script extract the spectrum from the central spaxel. The aperture correction is applied alone,
and also together with the so-called ``3x3 correction.'' This correction compares the ratio of the flux in the central spaxel to
the 3x3 ``super-spaxel'' to the ratio for a perfectly aligned point source. The ratio of ratios is the correction and is displayed
as a function of wavelength in Figure \ref{fig:3x3correction}.
\begin{figure}
\center\includegraphics[width=0.8\textwidth]{figures/ttau_3x3correction.pdf}
\caption{Diagnostic plot of the 3x3 correction computed within the extractCentralSpectrum task. The black curve is obtained by comparing
the ratio of the signal in the central spaxel to the the signal in the 3x3 central spaxels, and adjusting to that of a perfectly centred
point source. The green line is the median over all wavelengths. The correction is not to be trusted in this case because the
object is extended. The correction of 2.8 in the O I line indicates this line is even more spatially extended than the continuum. The dip
in the line at 63.2 microns shows this line is less spatially extended than the continuum.}
\label{fig:3x3correction}
\end{figure}

The resulting corrected spectra are shown in Figure \ref{fig:extractions}. Compared to broad-band measurements (such
as Akari 65 $\mu$m of 95 Jy), applying both the aperture correction and the 3x3 correction makes the continuum much too
bright. This overcorrection is understandable since we have seen that T Tauri has extended emission surrounding the central
point source. The ratio of ratios used in the 3x3 correction is badly misled by extended emission and should not be used
in this case.
\begin{figure}
    \begin{subfigure}[b]{0.5\textwidth}
        \includegraphics[width=\textwidth]{figures/ttau_finalcomparison.pdf}
        \caption{Central spectrum with corrections}\label{fig:final-full}
    \end{subfigure}
    \begin{subfigure}[b]{0.5\textwidth}
        \includegraphics[width=1.0\textwidth]{figures/extraction_zoom.pdf}
        \caption{Reprocessed and archive versions}\label{fig:extract_zoom}
    \end{subfigure}
\caption{Extracted spectra for T Tau with various corrections applied. The uncorrected spectrum (blue) is simply the
spectrum of the central spaxel from the reprocessed cube. For comparison, the uncorrected spectrum from the v10.3.0
cube from the archive (teal) shows a different wavelength sampling in the line but otherwise not much difference. Applying just the aperture correction (green) gives about
the right continuum flux for this object compared to broad-band measurements (about 95-100 Jy). Applying both the aperture correction
and the 3x3 correction (brown) makes the spectrum too bright. }
\label{fig:extractions}
\end{figure}


\end{document}

