
 
\date{\today}
\title{PACS Spectroscopy Recipe: \\Identifying and Accessing Data}

\documentclass[letterpaper,12pt, ]{article}
\usepackage[usenames,dvipsnames]{color}
\usepackage{tikz}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{wrapfig}
\usepackage{subcaption}
\usepackage{hyperref}

\textwidth=6.5in
\textheight=9.5in
\topmargin=-0.75in
\oddsidemargin=0.0in
\evensidemargin=0.0in

\pagestyle{myheadings}

\definecolor{light-gray}{gray}{0.90}
\definecolor{dark-gray}{gray}{0.4}
\definecolor{dark-blue}{RGB}{25,25,112}

\hypersetup{colorlinks=true,urlcolor=dark-blue}

\lstset{basicstyle=\normalsize\ttfamily,
        showstringspaces=false,
        basewidth=1.2ex,
        fontadjust=true,
        escapechar=�,
        backgroundcolor=\color{light-gray},
        stringstyle=\color{dark-gray},
        commentstyle=\color{black}
        numbers=left,
        numberstyle=\tiny,
        language=Python}

\newcommand{\roundbox}[1]{%
  \tikz[baseline=-1ex]%
  \node[%
  inner sep=1.5pt,
  draw=black,
  fill=black,
  text=white,
  rounded corners=2.5pt]{#1};}

\newcommand{\boxref}[1]{%
  \begingroup%
  \scriptsize\ttfamily%
  \roundbox{\ref{#1}}%
  \endgroup%
}

\newcount\mymark
\makeatletter
\def\mycoderef#1{%
  \global\advance\mymark by 1%
  \protected@write \@auxout {}{\string \newlabel {#1}{{\the\mymark}{}}}%
  \makebox[0pt][r]{{\scriptsize\roundbox{\the\mymark}~}}%
}
\makeatother



\begin{document}

\maketitle


\section{Introduction}\label{sec:intro}

This recipe covers the steps needed to identify a PACS spectroscopy observation, and
to gain access to the data. In this example we will use observations of a point source, HD 141569,
in the examples.


Before embarking on the steps in this recipe, the reader must should have an installation of the 
\href{http://herschel.esac.esa.int/HIPE_download.shtml}{Herschel Interactive Processing Environment (HIPE)} 
available and should be familiar with the \href{http://herschel.esac.esa.int/hcss-doc-12.0/load/quickstart/html/quickstart.html}{HIPE Quickstart Guide}.

A companion script contains the all the processing commands, so it is not necessary to copy and paste from this document into HIPE.

%%% Need to identify the routine checks to do no matter what.

\section{Identifying the observation in the Herschel Science Archive}\label{sec:id-archive}

Each observation performed by Herschel is identified by an Observation ID or obsid. You must first identify the
obsid for the data you would like to look at. Here are three methods you can use.

\subsection{Using the Herschel Science Archive User Interface}

The Herschel Science Archive User Interface (HUI) is a Java applet that enables direct access to the data, as well
as convenient display of ``browse products'' or thumbnails. The HUI can be launched from a HIPE session (following
\href{http://herschel.esac.esa.int/hcss-doc-12.0/load/quickstart/html/qs_open_hsa.html}{the instructions in the Quickstart Guide})
or \href{http://herschel.esac.esa.int/Science_Archive.shtml}{launching from a web browser}. From within the HUI, enter
HD 141569 as the target name and query the archive. You will see a number of photometry and spectroscopy observations.
For this recipe the PACS Line Spectroscopy observation of the O I 63$\mu$m line is selected, with obsid 1342190376.
Resist the temptation to download the tar file of all the data! The script for this recipe will download only those parts of
the observation that are needed and can save a great deal of downloading time.

\subsection{Using the Herschel Data Search at IRSA}

The Infrared Science Archive (IRSA) provides a web interface to search the archive at 
\href{http://irsa.ipac.caltech.edu/applications/Herschel/}{http://irsa.ipac.caltech.edu/applications/Herschel/}.
Type ``HD 141569'' into the Coordinate/Object box and set the search radius to 1 arcmin. A new browser tab
will appear with a table much like that shown in the HUI. Resist the temptation to click on the Observation ID
link! It will begin downloading a tarfile of all data in the observation.

\subsection{Using the Herschel Observation Log at Vizier}

The Herschel Observation Log may be search at Vizier at either the \href{http://vizier.u-strasbg.fr/viz-bin/VizieR?-source=VI\%2F139}{Strasbourg site}
or the \href{http://vizier.cfa.harvard.edu/viz-bin/VizieR?-source=VI\%2F139}{US mirror site}. Enter ``HD 141569'' as the object name
and a radius of 1 arcmin. The resulting table shows one PACS Line Spectroscopy observation with obsid = 1342190376.


\section{Accessing the data within HIPE}\label{sec:access}


The remainder of this recipe makes use of the companion script \lstinline$accessing_data.py$. Only selected lines from the
script are shown in this document. They are marked by line numbers to make them easy to find in the HIPE Editor view.

The observation ID is first set in HIPE using 
\lstinputlisting[language=Python, numbers=left, firstline=1, lastline=1, firstnumber=1]{accessing_data.py}


\subsection{Downloading everything with getObservation\\ and saveObservation}

In the interactive pipeline scripts in HIPE, you will find the ``classic'' usage of getObservation and saveObservation.
When the poolLocation and poolName are not specified, they default to the ``local store'' directory, and a
``data pool'' name (like a directory name) the same as the observation ID.

The observation is first loaded with
\lstinputlisting[language=Python, numbers=left, firstline=35, lastline=36, firstnumber=35]{accessing_data.py}

Before saving, it is recommended to turn on compression of the files in the pools. This can reduce
disk space by a factor of several. These lines turn on compression and save the configuration:
\lstinputlisting[language=Python, numbers=left, firstline=40, lastline=41, firstnumber=40]{accessing_data.py}

Then the observation is stored to a data pool named ``1342190376'' in the default location. The saving can
be done in the background, so that you can start working with the observation while the saving is going on.
\lstinputlisting[language=Python, numbers=left, firstline=45, lastline=45, firstnumber=45]{accessing_data.py}
\lstinputlisting[language=Python, numbers=left, firstline=47, lastline=45, firstnumber=47]{accessing_data.py}
The saveObservation command saves all the data associated with the observation, and can take tens of minutes
to complete. 

\subsection{Variation: Saving data on-demand from the archive ``cloud''}

It is not required to download or access every type of data associated with the observation. 
As an alternative to the classic use of getObservation and saveObservation, your HIPE system
can download products on-demand from the archive and store them in your MyHSA directory.
These script lines ensure that the archive is online and that as data products are accessed,
they will be stored on your machine.
\lstinputlisting[language=Python, numbers=left, label=lst:myhsa, firstline=58, lastline=59, firstnumber=58]{accessing_data.py}
The option to save data on-demand can also be set from the Edit - Preferences - My HSA menu, in the
Advanced tab.

The observation context  is loaded into HIPE with:
\lstinputlisting[language=Python, numbers=left, firstline=61, lastline=61, firstnumber=61]{accessing_data.py}

This method requires a connection to the HSA during processing, to allow data products to be accessed
``from the cloud'' when needed. {\bf Note:} if you are saving data on-demand, saving an observation to a pool
will effectively duplicate all the data to your MyHSA directory. When saving the entire observation to a pool,
you should leave 

\subsection{Logging into the HSA}

If you are not currently logged into the HSA, you may see an error like 
\begin{lstlisting}
herschel.ia.task.TaskException: Error processing getObservation task: 
  Not logged in (no credentials provided).
\end{lstlisting}
In this case, you should check the bottom status-bar of HIPE for the HSA log-in status, and if necessary, to click 
on the icon to log in with your Herschel username and password.

\section{Next steps}

At this stage, you may continue with one of the companion recipes (in suggested priority order):

Inspecting a Pointed Chopped Line Spectroscopy Observation 

Extracting a Spectrum and Line Parameters from a Point Source Observation

\end{document}

