
verbose = True
obsid = 1342190376
camera = "blue"
upsample = 4
oversample = 2
calTree = getCalTree()

# Coordinates in ICRS epoch J2010.0 from SIMBAD
ra_true = 237.49056479 
dec_true = -03.92125985

obs = getObservation(obsid=obsid, useHsa=True)

if (camera == "blue"):
    rebinnedCube = obs.level2.refs["HPS3DRB"].product.refs[0].product
else:
    rebinnedCube = obs.level2.refs["HPS3DRR"].product.refs[0].product
# The rebinning in the SPG 11.1.0 was done with oversample=2.0
# and upsample = 1.0. ipipe uses oversample=2 and upsample=4.
print rebinnedCube.meta['upsample']
print rebinnedCube.meta['oversample']

c1,c9,c129 = extractCentralSpectrum(rebinnedCube, \
    smoothing='median', isLineScan=-1, calTree=calTree, verbose=verbose)

# define a function to do the fit
def fitOneLine(spectrum, wavelength, verbose=False, engine=1, \
        calTree=getCalTree(), column=0, row=0):
    sf = SpectrumFitter(spectrum, column, row, verbose)
    #
    # Specify fit engine (1 = LevenbergMarquardt, 2 = Amoeba, 3 = Linear,
    # 4 = MP, 5 = Conjugated Gradient).
    #
    sf.useFitter(engine)
    #
    # Add the models and set the 'fix'ed.
    #
    continuum_guess = MEDIAN(spectrum.flux)
    order = spectrum.meta['order'].value
    resolution = getSpecResolution(order, Double1d([wavelength]), calTree)[0]
    c_speed = Constant.SPEED_OF_LIGHT.value*Constant.SPEED_OF_LIGHT.unit.to(Speed.KILOMETERS_PER_SECOND)
    width_guess = wavelength*resolution/c_speed/2.3548 #resolution is FWHM in km/sec
    M1 = sf.addModel('Polynomial', [0.0], [continuum_guess])
    M2 = sf.addModel('Gauss', [MAX(spectrum.flux),wavelength,width_guess])
    M1.setLimits([Double.NEGATIVE_INFINITY],[Double.POSITIVE_INFINITY])
    M2.setLimits([Double.NEGATIVE_INFINITY,Double.NEGATIVE_INFINITY,Double.NEGATIVE_INFINITY],\
        [Double.POSITIVE_INFINITY,Double.POSITIVE_INFINITY,Double.POSITIVE_INFINITY])
    sf.doGlobalFit()
    sf.residual()
    continuum = M1.fittedParameters[0]
    cont_err = M1.fittedStdDev[0]
    cont_unit = M1.fluxUnit
    peak, center, sigma = M2.fittedParameters
    peak_err, center_err, sigma_err = M2.fittedStdDev
    peak_unit = M2.fluxUnit
    sigma_unit = M2.waveUnit
    center_unit = M2.waveUnit
    intensity = M2.integral
    intensity_err = SQRT( (peak_err/peak)**2 + (sigma_err/sigma)**2 )*intensity
    # Want to output the line integral in SI units, W/m**2
    if center_unit.isCompatible(Length.METERS):
        conversion = sigma_unit.toSI*peak_unit.toSI*Constant.SPEED_OF_LIGHT.value\
                 /(center*center_unit.toSI)**2
    elif center_unit.isCompatible(Frequency.HERTZ):
        conversion = peak_unit.toSI*center_unit.toSI
    elif center_unit.isCompatible(WaveNumber.RECIPROCAL_METERS):
        conversion = peak_unit.toSI*Constant.SPEED_OF_LIGHT.value*center_unit.toSI
    intensity *= conversion
    intensity_err *= conversion
    intensity_unit = Flux.WATTS_PER_SQUARE_METER
    if (verbose):
        print 'Line center = %10.5g +/- %6.3g %s' % (center, center_err, center_unit)
        print 'Line peak =     %8.3g +/- %6.3g %s' % (peak, peak_err, peak_unit)
        print 'Line FWHM =     %8.3g +/- %6.3g %s' % (sigma*2.3548, sigma_err*2.3548, sigma_unit)
        print 'Continuum =     %8.3g +/- %6.3g %s' % (continuum, cont_err, cont_unit)
        print 'Line integral = %8.3g +/- %6.3g %s' % (intensity, intensity_err, intensity_unit)
    return(center, center_err, center_unit, peak, peak_err, peak_unit,
        sigma*2.3548, sigma_err*2.3548, sigma_unit,
        continuum, cont_err, cont_unit,
        intensity, intensity_err, intensity_unit)

rresults1 = fitOneLine(c1, 63.18, verbose=verbose)
rresults9 = fitOneLine(c9, 63.18, verbose=verbose)
rresults129 = fitOneLine(c129, 63.18, verbose=verbose)

# redo the wavelength regridding with better upsampling
if (camera == "blue"):
    slicedCubes = obs.level2.refs['HPS3DB'].product
else:
    slicedCubes = obs.level2.refs['HPS3DR'].product

# Rebin the cubes and combine them, then extract spectra
waveGrid=wavelengthGrid(slicedCubes, oversample=oversample, \
    upsample=upsample, calTree = calTree)
slicedRebinnedCubes = specWaveRebin(slicedCubes, waveGrid)
slicedFinalCubes = specAddNodCubes(slicedRebinnedCubes)
for slice in range(len(slicedFinalCubes.refs)):
    if verbose: print
    # a. Extract central spectrum, incl. point source correction (c1)
    # b. Extract SUM(central_3x3 spaxels), incl. point source correction (c9)
    # c. Scale central spaxel to the level of the central_3x3 (c129 -> See PDRG & print extractCentralSpectrum.__doc__)
    c1,c9,c129 = extractCentralSpectrum(slicedFinalCubes, slice=slice, \
            smoothing='median', isLineScan=-1, calTree=calTree, verbose=verbose)
    results1 = fitOneLine(c1, 63.18, verbose=verbose)
    results9 = fitOneLine(c9, 63.18, verbose=verbose)
    results129 = fitOneLine(c129, 63.18, verbose=verbose)

# Estimate the flux loss due to mis-pointing
ra_center = MEAN(cube.getRa()[:,2,2])
dec_center = MEAN(cube.getDec()[:,2,2])
raOffset = (ra_true-ra_center)*3600/COS(dec_center*3.14159/180.)
decOffset = (dec_true-dec_center)*3600
e2p1 = specExtendedToPointCorrection(cube, calTree, 0.05, 0.05,
      raOffset=raOffset, decOffset=decOffset)
correction = e2p1.flux[len(e2p1.flux)/2]
print 'Correction for mispointing is to divide by %.3f'%correction
correctedSpectrum = divide(ds1=c1, ds2=e2p1, overwrite=False)
